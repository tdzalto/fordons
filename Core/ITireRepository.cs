using System.Collections.Generic;
using System.Threading.Tasks;
using vega.Core.Models;

namespace vega.Core
{
    public interface ITireRepository
    {
         Task<Tire> GetTire(int id, bool includeRelated = true);
         void Add(Tire tire);
         void Remove(Tire tire);
         Task<QueryResult<Tire>> GetTires(TireQuery filter);
    }
}