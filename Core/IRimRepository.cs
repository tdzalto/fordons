using System.Collections.Generic;
using System.Threading.Tasks;
using vega.Core.Models;

namespace vega.Core
{
    public interface IRimRepository
    {
         Task<Rim> GetRim(int id, bool includeRelated = true);
         void Add(Rim rim);
         void Remove(Rim rim);
         Task<QueryResult<Rim>> GetRims(KeyValueQuery filter);
    }
}