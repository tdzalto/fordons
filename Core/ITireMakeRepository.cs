using System.Threading.Tasks;
using vega.Core.Models;

namespace vega.Core
{
    public interface ITireMakeRepository
    {
        Task<TireMake> GetTireMake(int id, bool includeRelated = true);
         void Add(TireMake tireMake);
         void Remove(TireMake tireMake);
         Task<QueryResult<TireMake>> GetTireMakes(DimQuery filter);
    }
}