using System.Collections.Generic;
using System.Threading.Tasks;
using vega.Core.Models;

namespace vega.Core
{
    public interface ITireDimRepository
    {
         Task<TireDim> GetTireDim(int id, bool includeRelated = true);
         void Add(TireDim tireDim);
         void Remove(TireDim tireDim);
         Task<QueryResult<TireDim>> GetTireDims(DimQuery filter);
    }
}