using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using vega.Core.Models;

namespace vega.Core
{
    public class PhotoService : IPhotoService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IPhotoStorage photoStorage;
        public PhotoService(IUnitOfWork unitOfWork, IPhotoStorage photoStorage)
        {
            this.photoStorage = photoStorage;
            this.unitOfWork = unitOfWork;
        }
        public async Task<VehiclePhoto> UploadPhoto(Vehicle vehicle, IFormFile file, string uploadsFolderPath)
        {
            var fileName = await photoStorage.StorePhoto(uploadsFolderPath, file);
            //System.Drawing

            var photo = new VehiclePhoto { FilePath = fileName, ThumbnailPath = "thumbnail/"+ fileName };
            vehicle.Photos.Add(photo);
            await unitOfWork.CompleteAsync();

            return photo;
        }
        public async Task<TirePhoto> UploadPhoto(Tire tire, IFormFile file, string uploadsFolderPath)
        {
            var fileName = await photoStorage.StorePhoto(uploadsFolderPath, file);
            //System.Drawing

            var photo = new TirePhoto { FilePath = fileName, ThumbnailPath = "thumbnail/"+ fileName };
            tire.Photos.Add(photo);
            await unitOfWork.CompleteAsync();

            return photo;
        }
        public async Task<RimPhoto> UploadPhoto(Rim rim, IFormFile file, string uploadsFolderPath)
        {
            var fileName = await photoStorage.StorePhoto(uploadsFolderPath, file);
            //System.Drawing

            var photo = new RimPhoto { FilePath = fileName, ThumbnailPath = "thumbnail/"+ fileName };
            rim.Photos.Add(photo);
            await unitOfWork.CompleteAsync();

            return photo;
        }

        public async Task<string> RemovePhoto(string filePath, string thumbnailPath)
        {
            var result = photoStorage.RemovePhoto(filePath, thumbnailPath);
            await unitOfWork.CompleteAsync();

            return result;
        }

    }
}