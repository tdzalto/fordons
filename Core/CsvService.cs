using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using vega.Core.Models;

namespace vega.Core
{
    public class CsvService : ICsvService
    {
        private readonly IUnitOfWork unitOfWork;
        public CsvService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public async Task<bool> UploadCsv()
        {
           await unitOfWork.CompleteAsync();
           return true;
        }

    }
}