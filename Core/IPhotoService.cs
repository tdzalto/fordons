using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using vega.Core.Models;

namespace vega.Core
{
    public interface IPhotoService
    {
         Task<VehiclePhoto> UploadPhoto(Vehicle vehicle, IFormFile file, string uploadsFolderPath);
         Task<TirePhoto> UploadPhoto(Tire tire, IFormFile file, string uploadsFolderPath);
         Task<RimPhoto> UploadPhoto(Rim rim, IFormFile file, string uploadsFolderPath);
         Task<string> RemovePhoto(string filePath, string thumbnailPath);
    }
}