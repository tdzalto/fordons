using System.Collections.Generic;
using System.Threading.Tasks;
using vega.Core.Models;

namespace vega.Core
{
    public interface IRimDimRepository
    {
         Task<RimDim> GetRimDim(int id, bool includeRelated = true);
         void Add(RimDim rimDim);
         void Remove(RimDim rimDim);
         Task<QueryResult<RimDim>> GetRimDims();
    }
}