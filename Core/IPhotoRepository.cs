using System.Collections.Generic;
using System.Threading.Tasks;
using vega.Core.Models;

namespace vega.Core
{
    public interface IPhotoRepository
    {
         Task<IEnumerable<VehiclePhoto>> GetVehiclePhotos(int vehicleId);
         Task<VehiclePhoto> GetVehiclePhoto(int vehicleId, int id);
         Task<IEnumerable<TirePhoto>> GetTirePhotos(int tireId);
         Task<TirePhoto> GetTirePhoto(int tireId, int id);
         Task<IEnumerable<RimPhoto>> GetRimPhotos(int rimId);
         Task<RimPhoto> GetRimPhoto(int rimId, int id);
         void RemoveVehiclePhoto(VehiclePhoto photo);
         void RemoveTirePhoto(TirePhoto photo);
         void RemoveRimPhoto(RimPhoto photo);
    }
}