using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SkiaSharp;

namespace vega.Core
{
    public class PhotoStorage : IPhotoStorage
    {
        public async Task<string> StorePhoto(string uploadsFolderPath, IFormFile file)
        {
            
            if (!Directory.Exists(uploadsFolderPath))
            {
                Directory.CreateDirectory(uploadsFolderPath);
            }

            var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
            var filePath = Path.Combine(uploadsFolderPath, fileName);
            

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream); // store the file in our system
            }

            ResizePhoto(filePath, uploadsFolderPath, file, fileName);

            return fileName;
        }

        public string RemovePhoto(string filePath, string thumbnailPath)
        {
           if(File.Exists(filePath))
            {
                File.Delete(filePath);
                    if(File.Exists(thumbnailPath))
                    {
                        File.Delete(thumbnailPath);
                        return "File and Thumbnail got removed";
                    }
                return "File got removed";
            }

            return "Request did not succeed";
        }

        const int size = 150;
        const int quality = 75;
        public void ResizePhoto(string filePath, string uploadsFolderPath, IFormFile file, string fileName)
        {
            using (var input = File.OpenRead(filePath))
            {
                using (var inputStream = new SKManagedStream(input))
                {
                    using (var original = SKBitmap.Decode(inputStream))
                    {
                        int width, height;
                        if (original.Width > original.Height)
                        {
                            width = size;
                            height = size; //original.Height * size / original.Width;
                        }
                        else
                        {
                            width = size; //original.Width * size / original.Height;
                            height = size;
                        }

                        using (var resized = original
                            .Resize(new SKImageInfo(width, height), SKBitmapResizeMethod.Lanczos3))
                        {
                            if (resized == null) return;

                            using (var image = SKImage.FromBitmap(resized))
                            {
                                using (var output = 
                                    File.OpenWrite(OutputPath(uploadsFolderPath + "/thumbnail/", fileName)))
                                {
                                    image.Encode(SKEncodedImageFormat.Jpeg, quality)
                                        .SaveTo(output);
                                }
                            }
                        }
                    }
                }
            }
        }

        public string OutputPath(string thumbnailPath, string fileName)
        {
            if (!Directory.Exists(thumbnailPath))
            {
                Directory.CreateDirectory(thumbnailPath);
            }
            return thumbnailPath + fileName;
        }


    }
}