using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("Models")]
    public class Model
    {
        public int Id {get; set;}
        [Required]
        [StringLength(255)]
        public string Name {get; set;}
        public Make Make { get; set; }
        public int MakeId { get; set; }
        public ICollection<ModelRimDim> RimDims { get; set; }
        public ICollection<ModelTireDim> TireDims { get; set; }
        public Model()
        {
            TireDims = new Collection<ModelTireDim>();
            RimDims = new Collection<ModelRimDim>();
        }
    }
}