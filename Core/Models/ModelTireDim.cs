using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("ModelTireDims")]
    public class ModelTireDim
    {
        public int ModelId { get; set; }
        public int TireDimId { get; set; }
        public Model Model { get; set; }
        public TireDim TireDim { get; set; }

    }
}