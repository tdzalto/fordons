using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("TirePhotos")]
    public class TirePhoto : Photo
    {
        public int TireId { get; set; }
    }
}