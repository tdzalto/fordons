using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("TireModels")]
    public class TireModel
    {
        public int Id {get; set;}
        [Required]
        [StringLength(255)]
        public string Name {get; set;}
        public TireMake TireMake { get; set; }
        public int TireMakeId { get; set; }
    }
}