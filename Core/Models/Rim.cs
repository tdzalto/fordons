using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("Rims")]
    public class Rim
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public int RimDimId { get; set; }
        public RimDim RimDim { get; set; }
        public int RimSize { get; set; }
        public string RimType { get; set; }
        public DateTime LastUpdate { get; set; }
        public int ContactId { get; set; }
        public Contact Contact { get; set; }
        public ICollection<RimPhoto> Photos { get; set; } //First build PhotoModel, this binds photoModel to Vehicle (2)

        public Rim()
        {
            Photos = new Collection<RimPhoto>();  //Initialize collection prop with creation of class (3)
        }

    }
}