using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("TireMakes")]
    public class TireMake
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public TireMakePhoto Logo { get; set; }
        public ICollection<TireModel> TireModels {get; set;}

        public TireMake()
        {
            TireModels = new Collection<TireModel>();
        }
    }
}