using System.Collections.Generic;
using System.Collections.ObjectModel;
using vega.Core.Models;

namespace vega.Controllers.Resources
{
    public class TireValuePair
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Tire> Tires { get; set; }
    }
}