using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("TireDims")]
    public class TireDim
    {
        public int Id { get; set; }

        public string Width { get; set; }
        public string Height { get; set; }
        public string Size { get; set; }
        public string Name {

            get {

                return Width + "/" + Height + "/" + Size; 
            }
        }
        public ICollection<Tire> Tires  { get; set; }
        public ICollection<ModelTireDim> Models  { get; set; }

        public TireDim()
        {
            Tires = new Collection<Tire>();
            Models = new Collection<ModelTireDim>();
        }

    }
}