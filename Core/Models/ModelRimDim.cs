using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("ModelRimDims")]
    public class ModelRimDim
    {
        public int ModelId { get; set; }
        public int RimDimId { get; set; }
        public Model Model { get; set; }
        public RimDim RimDim { get; set; }

    }
}