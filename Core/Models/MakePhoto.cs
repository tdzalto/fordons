using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("MakePhotos")]
    public class MakePhoto : Photo
    {
        public int MakeId { get; set; }
    }
}