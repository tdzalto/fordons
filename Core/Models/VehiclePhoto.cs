using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("VehiclePhotos")]
    public class VehiclePhoto : Photo
    {
        public int VehicleId { get; set; }
    }
}