using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("Contacts")]
    public class Contact
    {
        public int Id { get; set; }
        [Required]
        public string token { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public string Description { get; set; }
        public DateTime LastUpdate { get; set; }
        public ICollection<Vehicle> Vehicles { get; set; }
        public ICollection<Tire> Tires { get; set; }
        public ICollection<Rim> Rims { get; set; }
        public Contact()
        {
            Vehicles = new Collection<Vehicle>();
            Rims = new Collection<Rim>();
            Tires = new Collection<Tire>();

        }

    }
}