using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("Tires")]
    public class Tire
    {
        public int Id { get; set; }
        public string Name
        { 
            get
            {
                return  TireModel.TireMake.Name+" "+TireModel.Name+" - "+TireDim.Name;
            } 
        }
        public TireDim TireDim { get; set; }
        public int TireDimId { get; set; }
        public string TireType { get; set; }
        public DateTime LastUpdate { get; set; }
        public int ContactId { get; set; }
        public Contact Contact { get; set; }
        public TireModel TireModel { get; set; }
        public int TireModelId { get; set; }
        public ICollection<TirePhoto> Photos { get; set; } //First build PhotoModel, this binds photoModel to Vehicle (2)

        public Tire()
        {
            Photos = new Collection<TirePhoto>();  //Initialize collection prop with creation of class (3)
        }

    }
}