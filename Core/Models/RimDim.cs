using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("RimDims")]
    public class RimDim
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string Hub { get; set; }
        [Required]
        [StringLength(255)]
        public string BoltCircle { get; set; }
        [Required]
        [StringLength(255)]
        public string BoltNutDim { get; set; }
        [Required]
        [StringLength(255)]
        public string Offset { get; set; }
        public string Name 
        {
            get 
            {
                return Hub + " | " + BoltCircle + " | " + BoltNutDim + " | " + Offset; 
            }
        }
        public ICollection<Rim> Rims  { get; set; }

        public RimDim()
        {
            Rims = new Collection<Rim>();
        }

    }
}