using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("ContactPhotos")]
    public class ContactPhoto : Photo
    {
        public int ContactId { get; set; }
    }
}