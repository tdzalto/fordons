using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("TireMakePhotos")]
    public class TireMakePhoto : Photo
    {
        public int TireMakeId { get; set; }
    }
}