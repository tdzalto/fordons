using vega.Extensions;

namespace vega.Core.Models
{
    public class TireQuery : IQueryObject
    {
        public int? TireMakeId { get; set; }
        public int? TireModelId { get; set; }
        public string Name { get; set; }
        public string SortBy { get; set; }
        public bool IsSortAscending { get; set; }
        public int Page { get; set; }
        public byte PageSize { get; set; }
    }
}