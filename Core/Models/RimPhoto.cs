using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace vega.Core.Models
{
    [Table("RimPhotos")]
    public class RimPhoto : Photo
    {
        public int RimId { get; set; }
    }
}