using System.Collections.Generic;
using System.Threading.Tasks;
using vega.Core.Models;

namespace vega.Core
{
    public interface IContactRepository
    {
         Task<Contact> GetContact(int id, bool includeRelated = true);
         Task<Contact> GetContactByEmail(string email, bool includeRelated = true);
         Task<Contact> GetContactByToken(string token, bool includeRelated = true);
         void Add(Contact contact);
         Task<QueryResult<Contact>> GetContacts(KeyValueQuery filter);
    }
}