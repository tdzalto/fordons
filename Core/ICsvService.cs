using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using vega.Core.Models;

namespace vega.Core
{
    public interface ICsvService
    {
        Task<bool> UploadCsv();
    }
}