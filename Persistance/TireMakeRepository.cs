using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using vega.Core.Models;
using vega.Core;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;
using vega.Extensions;

namespace vega.Persistance
{
    public class TireMakeRepository : ITireMakeRepository
    {
        private readonly VegaDbContext context;
        public TireMakeRepository(VegaDbContext context)
        {
            this.context = context;
        }
        public async Task<TireMake> GetTireMake(int id, bool includeRelated = true)
        {
            if(!includeRelated)
                return await context.TireMakes.FindAsync(id);

            return await context.TireMakes
                .SingleOrDefaultAsync(v => v.Id == id);
        }

        public void Add(TireMake tireMake)
        {
            context.TireMakes.Add(tireMake);
        }

        public void Remove(TireMake tireMake)
        {
            context.TireMakes.Remove(tireMake);
        }

        public async Task<QueryResult<TireMake>> GetTireMakes(DimQuery queryObj)
        {
            var result = new QueryResult<TireMake>();
            var query = context.TireMakes
                .Include(t => t.TireModels)
                .AsQueryable();
            
            // query = query.ApplyFiltering(queryObj); there is no filtering for Tires

            result.TotalItems = await query.CountAsync();

            query = query.ApplyPaging(queryObj);

            result.Items = await query.ToListAsync();

            return result;
        }
    }
}