using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using vega.Core.Models;
using vega.Core;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;
using vega.Extensions;

namespace vega.Persistance
{
    public class TireRepository : ITireRepository
    {
        private readonly VegaDbContext context;
        public TireRepository(VegaDbContext context)
        {
            this.context = context;
        }
        public async Task<Tire> GetTire(int id, bool includeRelated = true)
        {
            if(!includeRelated)
                return await context.Tires.FindAsync(id);

            return await context.Tires
                .Include(t => t.TireModel)
                    .ThenInclude(tm => tm.TireMake)
                .Include(t => t.TireDim)
                .Include(t => t.Contact)
                .Include(t => t.Photos)
                .SingleOrDefaultAsync(v => v.Id == id);
        }

        public void Add(Tire tire)
        {
            context.Tires.Add(tire);
        }

        public void Remove(Tire tire)
        {
            context.Tires.Remove(tire);
        }

        public async Task<QueryResult<Tire>> GetTires(TireQuery queryObj)
        {
            var result = new QueryResult<Tire>();
            var query = context.Tires
                .Include(t => t.TireModel)
                    .ThenInclude(tm => tm.TireMake)
                .Include(t => t.TireDim)
                .Include(t => t.Contact)
                .Include(t => t.Photos)
                .AsQueryable();
            
            query = query.ApplyFiltering(queryObj);

            var columnsMap = new Dictionary<string, Expression<Func<Tire, object>>>
            {
                ["tireMake"] = t => t.TireModel.TireMake.Name,
                ["tireModel"] = t => t.TireModel.Name,
                ["contactName"] = t => t.Contact.Name
            };

            query = query.ApplyOrdering(queryObj, columnsMap);

            result.TotalItems = await query.CountAsync();

            query = query.ApplyPaging(queryObj);

            result.Items = await query.ToListAsync();

            return result;
        }
    }
}