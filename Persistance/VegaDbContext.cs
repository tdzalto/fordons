using Microsoft.EntityFrameworkCore;
using vega.Core.Models;

namespace vega.Persistance
{
    public class VegaDbContext:DbContext
    {
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Make> Makes { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<VehiclePhoto> VehiclePhotos { get; set; } // Add a Dbset for Photos (4)
        public DbSet<TirePhoto> TirePhotos { get; set; } // Add a Dbset for Photos (4)
        public DbSet<RimPhoto> RimPhotos { get; set; } // Add a Dbset for Photos (4)
        public DbSet<ContactPhoto> ContactPhotos { get; set; } // Add a Dbset for Photos (4)
        public DbSet<MakePhoto> MakePhotos { get; set; } // Add a Dbset for Photos (4)
        public DbSet<Rim> Rims { get; set; }
        public DbSet<RimDim> RimDims { get; set; }
        public DbSet<Tire> Tires { get; set; }
        public DbSet<TireMake> TireMakes { get; set; }
        public DbSet<TireModel> TireModels { get; set; }
        public DbSet<TireDim> TireDims { get; set; }
        public DbSet<CarSpec> CarSpecs { get; set; }
        public DbSet<Contact> Contacts { get; set; }

        public VegaDbContext(DbContextOptions<VegaDbContext> options)
        :base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //(below code is for many to many reletionship in Entity framework)
            // modelBuilder.Entity<Vehicle>()
            //     .HasMany<Feature>(v => v.Features)
            //     .WithMany(f => f.Vehicles)
            //     .Map(vf =>
            //             {
            //                 vf.MapLeftKey("VehicleId");
            //                 vf.MapRightKey("FeatureId");
            //                 vf.ToTable("VehicleFeature");
            //             });

            modelBuilder.Entity<VehicleFeature>().HasKey(vf => new { vf.VehicleId, vf.FeatureId });
            modelBuilder.Entity<ModelTireDim>().HasKey(mt => new { mt.ModelId, mt.TireDimId });
            modelBuilder.Entity<ModelRimDim>().HasKey(mr => new { mr.ModelId, mr.RimDimId });
        }

    }
}