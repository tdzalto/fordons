using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using vega.Core.Models;
using vega.Core;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;
using vega.Extensions;

namespace vega.Persistance
{
    public class RimDimRepository : IRimDimRepository
    {
        private readonly VegaDbContext context;
        public RimDimRepository(VegaDbContext context)
        {
            this.context = context;
        }
        public async Task<RimDim> GetRimDim(int id, bool includeRelated = true)
        {
            if(!includeRelated)
                return await context.RimDims.FindAsync(id);

            return await context.RimDims
                .SingleOrDefaultAsync(v => v.Id == id);
        }

        public void Add(RimDim rimDim)
        {
            context.RimDims.Add(rimDim);
        }

        public void Remove(RimDim rimDim)
        {
            context.RimDims.Remove(rimDim);
        }

        public async Task<QueryResult<RimDim>> GetRimDims()
        {
            var result = new QueryResult<RimDim>();
            var query = context.RimDims
            .Include(rd => rd.Rims)
                .AsQueryable();
            
            // query = query.ApplyFiltering(queryObj); there is no filtering for Tires

            result.TotalItems = await query.CountAsync();

            //query = query.ApplyPaging(queryObj);

            result.Items = await query.ToListAsync();

            return result;

        }
    }
}