using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using vega.Core.Models;
using vega.Core;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;
using vega.Extensions;

namespace vega.Persistance
{
    public class ContactRepository : IContactRepository
    {
        private readonly VegaDbContext context;
        public ContactRepository(VegaDbContext context)
        {
            this.context = context;
        }
        public async Task<Contact> GetContact(int id, bool includeRelated = true)
        {
            if(!includeRelated)
                return await context.Contacts.FindAsync(id);          

            return await context.Contacts
                .Include(c => c.Vehicles)
                    .ThenInclude(v => v.Model)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireModel)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireModel)
                    .ThenInclude(tm => tm.TireMake)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireDim)
                .Include(c => c.Rims)
                    .ThenInclude(t => t.RimDim)
                .SingleOrDefaultAsync(v => v.Id == id);
        }
        public async Task<Contact> GetContactByEmail(string email, bool includeRelated = true)
        {        

            return await context.Contacts
                .Include(c => c.Vehicles)
                    .ThenInclude(v => v.Model)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireModel)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireModel)
                    .ThenInclude(tm => tm.TireMake)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireDim)
                .Include(c => c.Rims)
                    .ThenInclude(t => t.RimDim)
                .SingleOrDefaultAsync(c => c.Email == email);
        }

        public async Task<Contact> GetContactByToken(string token, bool includeRelated = true)
        {        

            return await context.Contacts
                .Include(c => c.Vehicles)
                    .ThenInclude(v => v.Model)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireModel)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireModel)
                    .ThenInclude(tm => tm.TireMake)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireDim)
                .Include(c => c.Rims)
                    .ThenInclude(t => t.RimDim)
                .SingleOrDefaultAsync(c => c.token == token);
        }

        public void Add(Contact contact)
        {
            context.Contacts.Add(contact);
        }


        public async Task<QueryResult<Contact>> GetContacts(KeyValueQuery queryObj)
        {
            var result = new QueryResult<Contact>();
            var query = context.Contacts
                .Include(c => c.Vehicles)
                    .ThenInclude(v => v.Model)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireModel)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireModel)
                    .ThenInclude(tm => tm.TireMake)
                .Include(c => c.Tires)
                    .ThenInclude(t => t.TireDim)
                .Include(c => c.Rims)
                    .ThenInclude(t => t.RimDim)
                .AsQueryable();

            result.TotalItems = await query.CountAsync();

            query = query.ApplyPaging(queryObj);

            result.Items = await query.ToListAsync();

            return result;
        }
    }
}