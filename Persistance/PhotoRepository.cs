using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using vega.Core;
using vega.Core.Models;
using vega.Persistance;

namespace vega.Persistence
{
  public class PhotoRepository : IPhotoRepository
  {
    private readonly VegaDbContext context;
    public PhotoRepository(VegaDbContext context)
    {
      this.context = context;
    }
    public async Task<IEnumerable<VehiclePhoto>> GetVehiclePhotos(int vehicleId)
    {
      return await context.VehiclePhotos
        .Where(p => p.VehicleId == vehicleId)
        .ToListAsync();
    }

    public async Task<VehiclePhoto> GetVehiclePhoto(int vehicleId, int id)
    {
      return await context.VehiclePhotos
        .Where(p => p.VehicleId == vehicleId && p.Id == id)
        .SingleOrDefaultAsync();
    }
    public async Task<IEnumerable<TirePhoto>> GetTirePhotos(int tireId)
    {
      return await context.TirePhotos
        .Where(p => p.TireId == tireId)
        .ToListAsync();
    }

    public async Task<TirePhoto> GetTirePhoto(int tireId, int id)
    {
      return await context.TirePhotos
        .Where(p => p.TireId == tireId && p.Id == id)
        .SingleOrDefaultAsync();
    }
    public async Task<IEnumerable<RimPhoto>> GetRimPhotos(int rimId)
    {
      return await context.RimPhotos
        .Where(p => p.RimId == rimId)
        .ToListAsync();
    }

    public async Task<RimPhoto> GetRimPhoto(int rimId, int id)
    {
      return await context.RimPhotos
        .Where(p => p.RimId == rimId && p.Id == id)
        .SingleOrDefaultAsync();
    }

    public void RemoveVehiclePhoto(VehiclePhoto photo)
    {
        context.VehiclePhotos.Remove(photo);
    }
    public void RemoveTirePhoto(TirePhoto photo)
    {
        context.TirePhotos.Remove(photo);
    }
    public void RemoveRimPhoto(RimPhoto photo)
    {
        context.RimPhotos.Remove(photo);
    }
  }
}