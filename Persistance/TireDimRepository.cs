using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using vega.Core.Models;
using vega.Core;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;
using vega.Extensions;

namespace vega.Persistance
{
    public class TireDimRepository : ITireDimRepository
    {
        private readonly VegaDbContext context;
        public TireDimRepository(VegaDbContext context)
        {
            this.context = context;
        }
        public async Task<TireDim> GetTireDim(int id, bool includeRelated = true)
        {
            if(!includeRelated)
                return await context.TireDims.FindAsync(id);

            return await context.TireDims
                .SingleOrDefaultAsync(v => v.Id == id);
        }

        public void Add(TireDim tireDim)
        {
            context.TireDims.Add(tireDim);
        }

        public void Remove(TireDim tireDim)
        {
            context.TireDims.Remove(tireDim);
        }

        public async Task<QueryResult<TireDim>> GetTireDims(DimQuery queryObj)
        {
            var result = new QueryResult<TireDim>();
            var query = context.TireDims
                .Include(td => td.Tires)
                    .ThenInclude(t => t.TireModel)
                    .ThenInclude(tm => tm.TireMake)
                .AsQueryable();
            
            // query = query.ApplyFiltering(queryObj); there is no filtering for Tires

            result.TotalItems = await query.CountAsync();

            query = query.ApplyPaging(queryObj);

            result.Items = await query.ToListAsync();

            return result;
        }
    }
}