using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using vega.Core.Models;
using vega.Core;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;
using vega.Extensions;

namespace vega.Persistance
{
    public class ModelRepository : IModelRepository
    {
        private readonly VegaDbContext context;
        public ModelRepository(VegaDbContext context)
        {
            this.context = context;
        }
        public async Task<Model> GetModel(int id, bool includeRelated = true)
        {
            if(!includeRelated)
                return await context.Models.FindAsync(id);

            return await context.Models
                .SingleOrDefaultAsync(v => v.Id == id);
        }

        public void Add(Model model)
        {
            context.Models.Add(model);
        }

        public void Remove(Model model)
        {
            context.Models.Remove(model);
        }

        public async Task<QueryResult<Model>> GetModels(ModelQuery queryObj)
        {
            var result = new QueryResult<Model>();
            var query = context.Models
            .Include(m => m.Make)
            .Include(m => m.TireDims)
                .ThenInclude(mt => mt.TireDim)
            .Include(m => m.RimDims)
                .ThenInclude(mr => mr.RimDim)
            .AsQueryable();
            
            // query = query.ApplyFiltering(queryObj); there is no filtering for Tires

            result.TotalItems = await query.CountAsync();

            query = query.ApplyPaging(queryObj);

            result.Items = await query.ToListAsync();

            return result;

        }
    }
}