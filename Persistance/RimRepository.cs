using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using vega.Core.Models;
using vega.Core;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;
using vega.Extensions;

namespace vega.Persistance
{
    public class RimRepository : IRimRepository
    {
        private readonly VegaDbContext context;
        public RimRepository(VegaDbContext context)
        {
            this.context = context;
        }
        public async Task<Rim> GetRim(int id, bool includeRelated = true)
        {
            if(!includeRelated)
                return await context.Rims.FindAsync(id);

            return await context.Rims
                .Include(r => r.RimDim)
                .Include(r => r.Contact)
                .Include(r => r.Photos)
                .SingleOrDefaultAsync(v => v.Id == id);
        }

        public void Add(Rim rim)
        {
            context.Rims.Add(rim);
        }

        public void Remove(Rim rim)
        {
            context.Rims.Remove(rim);
        }

        public async Task<QueryResult<Rim>> GetRims(KeyValueQuery queryObj)
        {
            var result = new QueryResult<Rim>();
            var query = context.Rims
                .Include(r => r.RimDim)
                .Include(r => r.Contact)
                .Include(r => r.Photos)
                .AsQueryable();
            
            // query = query.ApplyFiltering(queryObj); there is no filtering for rims

            var columnsMap = new Dictionary<string, Expression<Func<Rim, object>>>
            {
                ["contactName"] = r => r.Contact.Name
            };

            query = query.ApplyOrdering(queryObj, columnsMap);

            result.TotalItems = await query.CountAsync();

            query = query.ApplyPaging(queryObj);

            result.Items = await query.ToListAsync();

            return result;
        }
    }
}