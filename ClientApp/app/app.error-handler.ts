import * as Raven from 'raven-js';
import { ToastyService } from 'ng2-toasty';
import { ErrorHandler, NgZone, Inject, isDevMode } from "@angular/core";


export class AppErrorHandler implements ErrorHandler {
    constructor(
        private ngZone: NgZone,
        @Inject(ToastyService) private toastyService: ToastyService){
    }

    handleError(error: any): void {
      this.ngZone.run(() => {
        if (typeof(window) !== 'undefined') {
        this.toastyService.error({
            title: 'Error',
            msg: 'Något gick snett.',
            theme: 'bootstrap',
            showClose: true,
            timeout: 5000
        });
       }
      });

      if(!isDevMode()){
        Raven.captureException(error.originalError || error);
      }
      else{
        throw error;
      }
    }
}