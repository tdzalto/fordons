export interface KeyValuePair{
    id: number;
    name: string;
}

export interface CarInfo {
    regnr: string;
    fullname: string;
    vehicle_year: number;
    model_year: number;
    color: string;
    chassi: string;
    transmission: string;
    fuel_1: string
    consumption_1: string;
    power_kw_1: number;
    power_hp_1: number;
    co2_1: string;
    nox_1: string;
    sound_level_1:number;
    imported: string;
    first_registered: string;
    first_on_swedish_roads: string;
    purchased: string;
    number_of_owners: number;
    meter: number;
    lastest_inspection: string;
    inspection_valid_until: string;
    tax: number;
    cylinder_volume: number;
    top_speed: string;
    four_wheel_drive: string;
    number_of_passengers: number;
    length: number;
    width: number;
    height: number;
    kerb_weight: number;
    total_weight: number;
    load_weight: number;
    passenger_airbag: string;

}

export interface Vehicle {
    id: number;
    model: KeyValuePair;
    make: KeyValuePair;
    isRegistered: boolean;
    features: KeyValuePair[];
    contactId: number;
    lastUpdate: string;
    
    regnr: string;
    fullname: string;
    vehicle_year: number;
    model_year: number;
    color: string;
    chassi: string;
    transmission: string;
    fuel_1: string
    consumption_1: string;
    power_kw_1: number;
    power_hp_1: number;
    co2_1: string;
    nox_1: string;
    sound_level_1:number;
    imported: string;
    first_registered: string;
    first_on_swedish_roads: string;
    purchased: string;
    number_of_owners: number;
    meter: number;
    lastest_inspection: string;
    inspection_valid_until: string;
    tax: number;
    cylinder_volume: number;
    top_speed: string;
    four_wheel_drive: string;
    number_of_passengers: number;
    length: number;
    width: number;
    height: number;
    kerb_weight: number;
    total_weight: number;
    load_weight: number;
    passenger_airbag: string;
}

export interface SaveVehicle {
    id: number;
    modelId: number;
    makeId: number;
    isRegistered: boolean;
    features: number[];
    contactId: number;
    
    regnr: string;
    fullname: string;
    vehicle_year: number;
    model_year: number;
    color: string;
    chassi: string;
    transmission: string;
    fuel_1: string
    consumption_1: string;
    power_kw_1: number;
    power_hp_1: number;
    co2_1: string;
    nox_1: string;
    sound_level_1:number;
    imported: string;
    first_registered: string;
    first_on_swedish_roads: string;
    purchased: string;
    number_of_owners: number;
    meter: number;
    lastest_inspection: string;
    inspection_valid_until: string;
    tax: number;
    cylinder_volume: number;
    top_speed: string;
    four_wheel_drive: string;
    number_of_passengers: number;
    length: number;
    width: number;
    height: number;
    kerb_weight: number;
    total_weight: number;
    load_weight: number;
    passenger_airbag: string;
}