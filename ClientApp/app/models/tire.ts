import { KeyValuePair, TireDim } from './tire';
export interface KeyValuePair{
    id: number;
    name: string;
}

export interface TireDim {
    width: string;
    height: string;
    size: string;
}

export interface Tire {
    id: number;
    tireModel: KeyValuePair;
    tireMake: KeyValuePair;
    tireDim: KeyValuePair;
    tireType: string;
    contactId: number;
    lastUpdate: string;

}

export interface SaveTire {
    id: number;
    tireModelId: number;
    tireMakeId: number;
    tireDimId: number;
    tireType: string;
    contactId: number;

}