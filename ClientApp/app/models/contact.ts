export interface KeyValuePair{
    id: number;
    name: string;
}

export interface Contact{
    id: number;
    token: string;
    name: string;
    address: string;
    postalCode: string;
    city:string
    phone: string;
    email: string;
    description: string;
}

export interface SaveContact{
    id: number;
    token: string;
    name: string;
    address: string;
    postalCode: string;
    city: string
    phone: string;
    email: string;
    description: string;
}

export interface Vehicle {
    id: number;
    model: KeyValuePair;
    make: KeyValuePair;
}