export interface KeyValuePair{
    id: number;
    name: string;
}


export interface Model {
    id: number;
    name: string;
    make: KeyValuePair;
    isRegistered: boolean;
    rimDims: KeyValuePair[];
    tireDims: KeyValuePair[];
    lastUpdate: string;
}

export interface SaveModel {
    id: number;
    name: string;
    makeId: number;
    isRegistered: boolean;
    tireDims: number[];
    rimDims: number[];
}