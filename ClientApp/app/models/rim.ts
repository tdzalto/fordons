import { KeyValuePair } from './vehicle';

export interface KeyValuePair {
    id: number;
    name: string;
}

export interface ContactApi {
    rims: Rim[];
  }

  export interface RimApi {
    rim: Rim[];
    totalItems: string;
  }


export interface Rim {
    id: number;
    name: string;
    rimDim: KeyValuePair;
    rimSize: number;
    rimType: string;
    contactId: number;
    lastUpdate: string;

}

export interface SaveRim {
    id: number;
    name: string;
    rimDimId: number;
    rimType: string;
    rimSize: number;
    contactId: number;
}