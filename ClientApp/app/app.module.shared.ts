import { LoadingOverlayComponent } from './core/loading-overlay/loading-overlay.component';
import { SidenavItemComponent } from './core/sidenav/sidenav-item/sidenav-item.component';
import { BreadcrumbsComponent } from './core/breadcrumb/breadcrumb.component';
import { QuickpanelComponent } from './core/quickpanel/quickpanel.component';
import { SearchBarComponent } from './core/toolbar/search-bar/search-bar.component';
import { ToolbarComponent } from './core/toolbar/toolbar.component';
import { SearchComponent } from './core/toolbar/search/search.component';
import { ToolbarUserButtonComponent } from './core/toolbar/toolbar-user-button/toolbar-user-button.component';
import { ToolbarNotificationsComponent } from './core/toolbar/toolbar-notifications/toolbar-notifications.component';
import { SidenavComponent } from './core/sidenav/sidenav.component';
import { ClickOutsideDirective } from './core/utils/click-outside.directive';
import { ScrollbarModule } from './core/scrollbar/scrollbar.module';
import { AdminComponent } from './core/admin/admin.component';
import { BreadcrumbService } from './core/breadcrumb/breadcrumb.service';
import { MediaReplayService } from './core/sidenav/mediareplay/media-replay.service';
import { SidenavService } from './core/sidenav/sidenav.service';
import { IconSidenavDirective } from './core/sidenav/icon-sidenav.directive';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { AppErrorHandler } from './app.error-handler';
import { AUTH_PROVIDERS, AuthHttp } from 'angular2-jwt';
import { AuthService } from './services/auth.service';
import { AdminAuthGuardService } from './services/admin-auth-guard.service';
import { AuthGuardService } from './services/auth-guard.service';
import { MaterialComponentsModule } from './material-components.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PaginationComponent } from './components/shared/pagination.component';
import { ChartModule } from 'angular2-chartjs';
import { ToastyModule } from 'ng2-toasty';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconRegistry} from "@angular/material";
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { JsonpModule, HttpModule, RequestOptions, Http } from '@angular/http';
import { CommonModule } from '@angular/common';
import { AppComponent } from './components/app/app.component';
import { RoutingModule, RoutingComponents } from './app-routing.module';
import { SortablejsModule, SortablejsOptions } from 'angular-sortablejs';
import * as Raven from 'raven-js';
import { NgModule, ErrorHandler } from '@angular/core';
import 'hammerjs';
import { HttpClient } from '@angular/common/http/src/client';
import { HttpClientModule } from '@angular/common/http';
import { authHttpServiceFactory } from './factories/authFactory';

Raven
    .config('https://2f8df759ab2d49b6bb48ca6d090a15d8@sentry.io/220664')
    .install();

@NgModule({
    declarations: [
        LoadingOverlayComponent,
        SidenavComponent,
        SidenavItemComponent,
        IconSidenavDirective,
        BreadcrumbsComponent,
        QuickpanelComponent,
        SearchBarComponent,
        ToolbarComponent,
        SearchComponent,
        ClickOutsideDirective,
        ToolbarUserButtonComponent,
        ToolbarNotificationsComponent,
        AppComponent,
        AdminComponent,
        SidenavComponent,
        PaginationComponent,
        RoutingComponents
    ],
    imports: [
        //UniversalModule,
        
        CommonModule,
        HttpModule,
        HttpClientModule,
        FormsModule,
        ToastyModule.forRoot(),
        ChartModule,
        BrowserAnimationsModule,
        BrowserModule,
        SortablejsModule,
        ScrollbarModule,
        MaterialComponentsModule,
        FlexLayoutModule,
        ReactiveFormsModule,
 
        //Set the routing for views
        RoutingModule,

    ],
    providers: [
        MatIconRegistry,
        { provide: ErrorHandler, useClass: AppErrorHandler },
        { provide: AuthHttp, useFactory: authHttpServiceFactory, deps: [Http, RequestOptions] },
        AdminAuthGuardService,
        AuthService,
        AuthGuardService,
        SidenavService,
        MediaReplayService,
        BreadcrumbService
    ],

})
export class AppModuleShared {
}
