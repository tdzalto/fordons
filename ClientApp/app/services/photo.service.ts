import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';

@Injectable()
export class PhotoService {

    constructor(private http: Http, private authHttp: AuthHttp) { 
    }

    getVehiclePhotos(vehicleId)
    {
        return this.http.get(`/api/vehicles/${vehicleId}/photos`)
            .map(res => res.json());
    }
    getTirePhotos(tireId)
    {
        return this.http.get(`/api/tires/${tireId}/photos`)
            .map(res => res.json());
    }
    getRimPhotos(rimId)
    {
        return this.http.get(`/api/rims/${rimId}/photos`)
            .map(res => res.json());
    }

    //Create a service for the API (POST method) the response is a Json (9)
    uploadVehiclePhoto(vehicleId, photo) { 
        var formData = new FormData(); // nativ javascript object
        formData.append('file', photo); // keyvaluepair, in photoscontroller we used (IFormFile file)
        return this.http.post(`/api/vehicles/${vehicleId}/photos`, formData)
            .map(res => res.json());
    }
    uploadTirePhoto(tireId, photo) { 
        var formData = new FormData(); // nativ javascript object
        formData.append('file', photo); // keyvaluepair, in photoscontroller we used (IFormFile file)
        return this.http.post(`/api/tires/${tireId}/photos`, formData)
            .map(res => res.json());
    }
    uploadRimPhoto(rimId, photo) { 
        var formData = new FormData(); // nativ javascript object
        formData.append('file', photo); // keyvaluepair, in photoscontroller we used (IFormFile file)
        return this.http.post(`/api/rims/${rimId}/photos`, formData)
            .map(res => res.json());
    }

    deleteVehiclePhoto(vehicleId, id) {
        return this.http.delete(`/api/vehicles/${vehicleId}/photos/${id}`)
          .map(res => res.json());
      }
    deleteTirePhoto(tireId, id) {
        return this.http.delete(`/api/tires/${tireId}/photos/${id}`)
          .map(res => res.json());
      }
    deleteRimPhoto(rimId, id) {
        return this.http.delete(`/api/rims/${rimId}/photos/${id}`)
          .map(res => res.json());
      }
}