import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthHttp } from 'angular2-jwt';


@Injectable()
export class BilUppgifterService {
  private readonly BilUppgifterEndpoint = '/api/fordons/';
  constructor(private http: Http, private authHttp: AuthHttp) { }

  getCarInfo(reg){
    return this.authHttp.request(this.BilUppgifterEndpoint + reg)
      .map(res => {
        return res.json()
      });
  }
}