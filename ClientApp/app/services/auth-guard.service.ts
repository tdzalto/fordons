import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(protected auth: AuthService) { }

    canActivate() {
        if(this.auth.isAuthenticated())
            return true;

        window.location.href = "https://fordon.eu.auth0.com/login?client=1Gj9YwH4NVNrDPnp7ahqoHku7SGmGTmC";
        return false;

    }
}