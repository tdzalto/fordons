import { Observable } from 'rxjs/Observable';
import { SaveContact } from './../models/contact';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/toPromise';
import { AuthHttp } from 'angular2-jwt';

@Injectable()
export class ContactService {
  private readonly contactEndpoint = '/api/contacts';
  constructor(private http: Http, private authHttp: AuthHttp) { }

  create(contact){
    return this.authHttp.post(this.contactEndpoint, contact)
      .map(res => res.json());
  }

  getContactByEmail(email){
    return this.authHttp.get(this.contactEndpoint + '/email/' + email)
      .map(res => res.json());
  }
  
  getContactByToken(token){
    return this.authHttp.get(this.contactEndpoint + '/token/' + token)
      .map(res => res.json());
  }
  
  getContactStatusByEmail(email){
    return this.authHttp.get(this.contactEndpoint + '/email/' + email)
      .map(res => res.json())
      .subscribe(x => {
        return true; 
      },
      err => {
        if(err.status == 404){
          return false;
          }
        })
  }

  getContacts(){
    return this.authHttp.get(this.contactEndpoint)
      .map(res => res.json().items);
  }

  update(contact: SaveContact) {
    return this.authHttp.put(this.contactEndpoint + '/' + contact.id, contact)
      .map(res => res.json());
  }

}
