
import { SaveRim } from './../models/rim';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthHttp } from 'angular2-jwt';

@Injectable()
export class RimService {
  private readonly rimEndpoint = '/api/rims';
  constructor(private http: Http, private authHttp: AuthHttp) { }


  create(rim){
    return this.authHttp.post(this.rimEndpoint, rim)
      .map(res => res.json());
  }

  getRimDims(){
    return this.http.get('/api/rimDims')
      .map(res => res.json());
  }

  getRim(id){
    return this.http.get(this.rimEndpoint + '/' + id)
      .map(res => res.json());
  }

  getRims(filter){
    return this.http.get(this.rimEndpoint + '?' + this.toQueryString(filter))
      .map(res => res.json());
  }

  toQueryString(obj){
    var parts = [];
    for(var property in obj){
      var value = obj[property];
      if(value != null && value != undefined)
        parts.push(encodeURIComponent(property) + '=' + encodeURIComponent(value));
    }

    return parts.join('&');
  }

  update(rimDim: SaveRim) {
    return this.authHttp.put(this.rimEndpoint + '/' + rimDim.id, rimDim)
      .map(res => res.json());
  }

  delete(id){
    return this.authHttp.delete(this.rimEndpoint + '/' + id)
      .map(res => res.json());
  }

}
