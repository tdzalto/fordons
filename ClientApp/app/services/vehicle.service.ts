import { SaveVehicle } from './../models/vehicle';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthHttp } from 'angular2-jwt';

@Injectable()
export class VehicleService {
  private readonly vehicleEndpoint = '/api/vehicles';
  constructor(private http: Http, private authHttp: AuthHttp) { }

  getMakes(){
    return this.http.get('/api/makes')
      .map(res => res.json());
  }

  getTireDims(){
    return this.http.get('/api/tireDims')
      .map(res => res.json());
  }

  getFeatures(){
    return this.http.get('/api/features')
      .map(res => res.json());
  }

  getRims(){
    return this.http.get('/api/rims')
      .map(res => res.json());
  }

  create(vehicle){
    return this.authHttp.post(this.vehicleEndpoint, vehicle)
      .map(res => res.json());
  }

  getVehicle(id){
    return this.http.get(this.vehicleEndpoint + '/' + id)
      .map(res => res.json());
  }

  getVehicles(filter){
    return this.http.get(this.vehicleEndpoint + '?' + this.toQueryString(filter))
      .map(res => res.json());
  }

  toQueryString(obj){
    var parts = [];
    for(var property in obj){
      var value = obj[property];
      if(value != null && value != undefined)
        parts.push(encodeURIComponent(property) + '=' + encodeURIComponent(value));
    }

    return parts.join('&');
  }

  update(vehicle: SaveVehicle) {
    return this.authHttp.put(this.vehicleEndpoint + '/' + vehicle.id, vehicle)
      .map(res => res.json());
  }

  delete(id){
    return this.authHttp.delete(this.vehicleEndpoint + '/' + id)
      .map(res => res.json());
  }

}
