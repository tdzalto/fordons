import { ToastyService } from 'ng2-toasty';
import { Observable } from 'rxjs/Observable';
import { SaveContact } from './../models/contact';
import { ContactService } from './contact.service';
// src/app/auth/auth.service.ts

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import * as auth0 from 'auth0-js';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class AuthService {
  id: string;
  profile: any;
  private roles: string[] = [];
  contact: SaveContact = {
    id: 0,
    token:"",
    name:"",
    email: "",
    phone: "",
    address: "",
    postalCode:"",
    city:"",
    description:"",
  };

  auth0 = new auth0.WebAuth({
    clientID: '1Gj9YwH4NVNrDPnp7ahqoHku7SGmGTmC',
    domain: 'fordon.eu.auth0.com',
    responseType: 'token id_token',
    audience: 'https//api.fordon.se',
    redirectUri: 'http://35.197.216.216/',      
    scope: 'openid email profile'
  });

  constructor(public router: Router,
              public contactService: ContactService,
              private toastyService: ToastyService) {

    this.readUserFromLocalStorage();
    
  }

  public getProfile(cb):void {
    const accessToken = localStorage.getItem('token');
    if (!accessToken) {
      throw new Error('Access token must exist to fetch profile');
    }

    this.auth0.client.userInfo(accessToken, (err, profile) => {
      if (profile) {
        this.profile = profile;
      }
      cb(err, profile);
    });
  }

  public updateUserSession(email, name, token):void {
      try {
      this.contactService.getContactByEmail(email)
      .subscribe(user => {
          this.contact.id = user.id;
          this.contact.name = user.name;
          this.contact.email = user.email;
          this.contact.phone = user.phone;
          this.contact.address = user.address;
          this.contact.postalCode = user.postalCode;
          this.contact.city = user.city;
          this.contact.description = user.description;
          this.contact.token = token
          this.submit();
      },
        err => {
          this.contact.name = name
          this.contact.token = token
          this.contact.email = email
          this.submit();
        }) 
      } catch(err){

      } 
  }

  submit() {
    var result$ = (this.contact.id) ? this.contactService.update(this.contact) : this.contactService.create(this.contact); 
    result$.subscribe(contact => {
      this.toastyService.success({
        title: 'Success', 
        msg: 'Data was sucessfully saved.',
        theme: 'bootstrap',
        showClose: true,
        timeout: 5000
      });
    });
  }


  private readUserFromLocalStorage() {
    this.profile = JSON.parse(localStorage.getItem('profile'));
    var token = localStorage.getItem('token');

    if(token) {
      var jwtHelper = new JwtHelper();
      var decodedToken = jwtHelper.decodeToken(token);
      this.roles = decodedToken['https://fordon.com/roles'] || [];
      }
  }

  public isInRole(roleName) {
    return this.roles.indexOf(roleName) > -1;
  }

  public login(): void {
    this.auth0.authorize();
  }

  // src/app/auth/auth.service.ts

  public handleAuthentication(): void {
    this.auth0.parseHash((err, authResult) => {
      console.log(authResult);
      if (authResult && authResult.accessToken) {
        window.location.hash = '';
        this.setSession(authResult);
        this.readUserFromLocalStorage();
        this.router.navigate(['/']);
      } else if (err) {
        this.router.navigate(['/home']);
        console.log(err);
      }
    });
  }

  private setSession(authResult): void {
    this.auth0.client.userInfo(authResult.accessToken, (err, profile) => {
        this.profile = JSON.stringify(profile);
        this.updateUserSession(profile['email'], profile['name'], authResult.accessToken);

    });
    localStorage.setItem('token', authResult.accessToken);
    
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('expires_at', expiresAt);
  }

  public logout(): void {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('profile');
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');
    this.profile = null;
    this.roles = [];
    // Go back to login again
    this.auth0.authorize();
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  public delay(t) {
    return new Promise(function(resolve) { 
        setTimeout(resolve, t)
    });
 }

}
