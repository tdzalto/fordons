import { SaveTire } from './../models/tire';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthHttp } from 'angular2-jwt';

@Injectable()
export class TireService {
  private readonly tireEndpoint = '/api/tires';
  constructor(private http: Http, private authHttp: AuthHttp) { }


  create(tire){
    return this.authHttp.post(this.tireEndpoint, tire)
      .map(res => res.json());
  }

  getTireMakes(){
    return this.http.get('/api/tireMakes')
      .map(res => res.json());
  }

  getTireDims(){
    return this.http.get('/api/tireDims')
      .map(res => res.json());
  }

  getTire(id){
    return this.http.get(this.tireEndpoint + '/' + id)
      .map(res => res.json());
  }

  getTires(filter){
    return this.http.get(this.tireEndpoint + '?' + this.toQueryString(filter))
      .map(res => res.json());
  }

  toQueryString(obj){
    var parts = [];
    for(var property in obj){
      var value = obj[property];
      if(value != null && value != undefined)
        parts.push(encodeURIComponent(property) + '=' + encodeURIComponent(value));
    }

    return parts.join('&');
  }

  update(tire: SaveTire) {
    return this.authHttp.put(this.tireEndpoint + '/' + tire.id, tire)
      .map(res => res.json());
  }

  delete(id){
    return this.authHttp.delete(this.tireEndpoint + '/' + id)
      .map(res => res.json());
  }

}
