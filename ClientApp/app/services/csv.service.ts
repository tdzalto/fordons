import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CsvService {

    constructor(private http: Http) { 
    }

    //Create a service for the API (POST method) the response is a Json (9)
    upload(csvFile, type) { 
        var formData = new FormData(); // nativ javascript object
        formData.append('file', csvFile); // keyvaluepair, in photoscontroller we used (IFormFile file)
        return this.http.post(`/api/${type}`, formData)
            .map(res => res.json());
    }

}