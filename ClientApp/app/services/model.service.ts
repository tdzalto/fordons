import { SaveModel } from './../models/model';
import { SaveVehicle } from './../models/vehicle';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthHttp } from 'angular2-jwt';

@Injectable()
export class ModelService {
  private readonly modelEndpoint = '/api/models';
  constructor(private http: Http, private authHttp: AuthHttp) { }

  getMakes(){
    return this.http.get('/api/makes')
      .map(res => res.json());
  }

  getTireDims(){
    return this.http.get('/api/tireDims')
      .map(res => res.json());
  }

  getFeatures(){
    return this.http.get('/api/features')
      .map(res => res.json());
  }

  getRims(){
    return this.http.get('/api/rims')
      .map(res => res.json());
  }

  create(model){
    return this.authHttp.post(this.modelEndpoint, model)
      .map(res => res.json());
  }

  getModel(id){
    return this.http.get(this.modelEndpoint + '/' + id)
      .map(res => res.json());
  }

  getModels(filter){
    return this.http.get(this.modelEndpoint + '?' + this.toQueryString(filter))
      .map(res => res.json());
  }

  toQueryString(obj){
    var parts = [];
    for(var property in obj){
      var value = obj[property];
      if(value != null && value != undefined)
        parts.push(encodeURIComponent(property) + '=' + encodeURIComponent(value));
    }

    return parts.join('&');
  }

  update(model: SaveModel) {
    return this.authHttp.put(this.modelEndpoint + '/' + model.id, model)
      .map(res => res.json());
  }

  delete(id){
    return this.authHttp.delete(this.modelEndpoint + '/' + id)
      .map(res => res.json());
  }

}
