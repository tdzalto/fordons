import { Contact } from './../../models/contact';
import { ContactService } from './../../services/contact.service';
import { Observable } from 'rxjs/Observable';
import { routeAnimation } from './../../route.animation';
import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Vehicle, KeyValuePair } from './../../models/vehicle';
import { VehicleService } from './../../services/vehicle.service';
import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { ResizeSensor } from 'css-element-queries';
import { merge } from 'rxjs/observable/merge';
import { catchError } from 'rxjs/operators/catchError';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { startWith } from 'rxjs/operators/startWith';
import { switchMap } from 'rxjs/operators/switchMap';
import { map } from 'rxjs/operators/map';
import {of as observableOf} from 'rxjs/observable/of';


@Component({
    selector: 'table-http-example',
    templateUrl: 'vehicle-list.component.html',
    styleUrls: ['./vehicle-list.component.scss'],
})

export class VehicleListComponent implements AfterViewInit {

        displayedColumns = [ 'id', 'lastUpdate', 'name', 'isRegistered'];
        dataSource = new MatTableDataSource();
        resultsLength = 0;
        isLoadingResults = false;
        isRateLimitReached = false;
        user:any;

        tableHover: boolean = true;
        tableStriped: boolean = true;
        tableCondensed: boolean = true;
        tableBordered: boolean = true;

      
        @ViewChild(MatPaginator) paginator: MatPaginator;
        @ViewChild(MatSort) sort: MatSort;

        constructor(private vehicleService: VehicleService,
          private authService: AuthService,
          private router: Router,
          private contactService: ContactService) { }
      
        ngAfterViewInit() {
          // If the user changes the sort order, reset back to the first page.
          this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
      
          merge(this.sort.sortChange, this.paginator.page)
            .pipe(
              startWith({}),
              switchMap(() => {
                this.isLoadingResults = false;
                return this.contactService.getContactByToken(localStorage.getItem('token'));
              }),
              map(data => {
                // Flip flag to show that loading has finished.
                this.isLoadingResults = false;
                this.isRateLimitReached = false;
                //this.resultsLength = data.totalItems;
                return data.vehicles;
              }),
              catchError(() => {
                this.isLoadingResults = false;
                // Catch if the GitHub API has reached its rate limit. Return empty data.
                this.isRateLimitReached = false;
                return observableOf([]);
              })
            ).subscribe(data => this.dataSource.data = data);
        }
      }
      

// import { routeAnimation } from './../../route.animation';
// import { ContactService } from './../../services/contact.service';
// import { Router } from '@angular/router';
// import { AuthService } from './../../services/auth.service';
// import { Vehicle, KeyValuePair } from './../../models/vehicle';
// import { VehicleService } from './../../services/vehicle.service';
// import { Component, OnInit, ElementRef, ViewChild, AfterViewInit} from '@angular/core';
// import { ResizeSensor } from 'css-element-queries';


// @Component({
//     selector: 'ms-vehicle-list',
//     templateUrl: 'vehicle-list.component.html',
//     styleUrls: ['./vehicle-list.component.scss'],
//     host: {
//       '[@routeAnimation]': 'true'
//     },
//     animations: [ routeAnimation ]
// })

// export class VehicleListComponent implements OnInit{
//     private readonly PAGE_SIZE = 3;

//     @ViewChild('tbody')
//     tbody: ElementRef;

//     cellWidths = [];

//     tableHover: boolean = true;
//     tableStriped: boolean = true;
//     tableCondensed: boolean = true;
//     tableBordered: boolean = true;

//     profile: any;
//     email: "";
//     queryResult: any ={"items":[]};
//     makes: KeyValuePair[];
//     query: any = {
//         pageSize: this.PAGE_SIZE
//     };
//     columns = [
//         { title: 'Id' },
//         { title: 'Namn', key: 'make', isSortable: true },
//     ];

//     constructor(private vehicleService: VehicleService,
//         private contactService: ContactService,
//          private authService: AuthService,
//          private router: Router,) { }

//          renderTableHead() {

//             let cells = this.tbody.nativeElement.children[0].children;
        
//             for (let cell of cells) {
//               this.cellWidths.push(cell.offsetWidth);
//             }
        
//             let resizeSensor = new ResizeSensor(this.tbody.nativeElement, () => {
//               this.cellWidths.length = 0;
        
//               for (let cell of cells) {
//                 this.cellWidths.push(cell.offsetWidth);
//                 }
//                 });
//             }    

//     ngOnInit() {
//         this.vehicleService.getMakes()
//             .subscribe(makes => this.makes = makes);

//             this.getProfile();
//     }
        
//             private getProfile(): Promise<void> {
//                 return new Promise((resolve, reject) => {
//                     if (this.authService.profile) {
//                         this.profile = this.authService.profile
//                       } else {
//                     this.authService.getProfile((err, profile) => {
//                         this.profile = profile;
//                       });
//                     }
            
//                   if(this.profile != null){
//                       this.email = this.profile.email;
//                         resolve();
//                   }else{
//                     this.authService.delay(2000).then(() =>{
//                       if(this.profile != null){
//                         this.email = this.profile.email;
//                         resolve();
//                       }else{
//                         reject();
//                       }}
//                     )
//                   } 
//                   })
//                   .then(() => { 
//                         this.populateVehicles();
//                     },
//                     err => {
//                       if(err.status == 404)
//                         this.router.navigate(['/home']); // should go to a route "not-found"
//                     })
//                   .catch(() => { 
//                     console.log("Promise to return profile is rejected!");
//                   });
//                 }
                
                
//     private populateVehicles(){
//         this.contactService.getContactByEmail(this.email)
//             .subscribe(result => {
//                 if(result.totalItems > 1){
//                  this.queryResult["items"] = result.items.filter(item =>
//                      item.contact.email == this.email).vehicles;
//                 }else{
//                   this.queryResult["items"] = result.vehicles;
//                   this.authService.delay(500).then(() => this.renderTableHead());
//                     }
//                 }
//             )};

//     onFilterChange(){
//         this.query.page = 1;
//         this.populateVehicles();
//     }

//     resetFilter() {
//         this.query = {
//             page: 1,
//             pageSize: this.PAGE_SIZE
//         };
//         this.populateVehicles();
//     }

//     sortBy(columnName){
//         if(this.query.sortBy === columnName){
//             this.query.isSortAscending = !this.query.isSortAscending;
//         }else{
//             this.query.sortBy = columnName;
//             this.query.isSortAscending = true;
//         }
//         this.populateVehicles();
//     }

//     onPageChange(page){
//         this.query.page = page;
//         this.populateVehicles();
//     }
// }