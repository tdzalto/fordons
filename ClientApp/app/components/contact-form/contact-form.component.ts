import { AuthService } from './../../services/auth.service';
import { ContactService } from './../../services/contact.service';
import { SaveContact, Contact } from './../../models/contact';
import * as _ from 'underscore';
import { Component, OnInit } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  //styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {
  profile: any;
  user: any;
  name: "";
  email: "";
  token:"";
  contact: SaveContact = {
    id: 0,
    token:"",
    name:"",
    email: "",
    phone: "",
    address: "",
    postalCode:"",
    city:"",
    description:"",
  };
 

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private contactService: ContactService,
    private toastyService: ToastyService) {

    

    }

  ngOnInit() {
    this.getProfile();

  }

  public getProfile(): void {
        this.contactService.getContactByToken(localStorage.getItem('token'))
        .subscribe(x => {
          this.user = x;
          if(this.user != null)
            this.setContact(this.user);
        },
        err => {
          if(err.status == 404){
            this.authService.login(); 
            console.log('you session has expired');
          }
        });
    }

  private setContact(c: Contact){
    try{
    this.contact.id = c.id;
    this.contact.email = c.email;
    this.contact.name = c.name;
    this.contact.phone = c.phone;
    this.contact.address = c.address;
    this.contact.postalCode = c.postalCode;
    this.contact.city = c.city;
    this.contact.description = c.description;
    }catch(err){
      console.log("session expired");
    }
  }


  submit() {
    var result$ = (this.contact.id) ? this.contactService.update(this.contact) : this.contactService.create(this.contact); 
    result$.subscribe(contact => {
      this.toastyService.success({
        title: 'Success', 
        msg: 'Data was sucessfully saved.',
        theme: 'bootstrap',
        showClose: true,
        timeout: 5000
      });
      this.router.navigate(['/contact'])
    });
  }
}
