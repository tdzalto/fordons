import { routeAnimation } from './../../route.animation';
import { AuthService } from './../../services/auth.service';
import { ContactService } from './../../services/contact.service';
import * as _ from 'underscore';
import { SaveTire, Tire } from './../../models/tire';
import { TireService } from './../../services/tire.service';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-tire-form',
  templateUrl: './tire-form.component.html',
  styleUrls: ['./tire-form.component.scss'],
  host: {
    '[@routeAnimation]': 'true'
  },
  animations: [ routeAnimation ]
})


export class TireFormComponent implements OnInit {
  @ViewChild('tireMakeInput') tireMakeInput: ElementRef;
  @ViewChild('tireModelInput') tireModelInput: ElementRef;
  @ViewChild('tireDimInput') tireDimInput: ElementRef;
  currentTireMake = '';
  reactiveTireMakeAuto: any;
  currentTireModel = '';
  reactiveTireModelAuto: any;
  currentTireDim = '';
  reactiveTireDimAuto: any;
  tireMakeCtrl: FormControl;
  reactiveTireMakes: any;
  tireModelCtrl: FormControl;
  reactiveTireModels: any;
  tireDimCtrl: FormControl;
  reactiveTireDims: any;

  email:"";
  tireMakes: any[];
  tireModels: any[];
  tireDims: any[];
  tire: SaveTire = {
    id: 0,
    tireModelId: 0,
    tireMakeId: 0,
    tireType: "summer",
    tireDimId: 0,
    contactId:0,
  };
 

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private contactService: ContactService,
    private authService: AuthService,
    private tireService: TireService,
    private toastyService: ToastyService) { 

      route.params.subscribe(p => {
        this.tire.id = +p['id'] || 0; //puts + to convert to a number
      })

      this.tireMakeCtrl = new FormControl({id: 0, name: '', tireModels:[]});
      this.reactiveTireMakes = this.tireMakeCtrl.valueChanges
        .startWith(this.tireMakeCtrl.value)
        .map(val => this.displayFn(val))
        .map(name => this.filterTireMakes(name));

      this.tireDimCtrl = new FormControl({id: 0, name:''});
      this.reactiveTireDims = this.tireDimCtrl.valueChanges
        .startWith(this.tireDimCtrl.value)
        .map(val => this.displayFn(val))
        .map(name => this.filterTireDims(name));
    }

  ngOnInit() {
    var sources = [
      //add sources
      this.tireService.getTireMakes(),
      this.tireService.getTireDims(),
      this.contactService.getContactByToken(localStorage.getItem('token')),
    ];
  

    if(this.tire.id){
      sources.push(this.tireService.getTire(this.tire.id));
    }

    //Putting all observables in an array and getting them by the index. 
    Observable.forkJoin(sources).subscribe(data => {
      this.tireMakes = data[0];
      this.tireDims = data[1];
      this.tire.contactId = data[2].id;

      if(this.tire.id){
        this.setTire(data[3]);
        //this.tireMakeCtrl = new FormControl({id: this.tire.id, name: this.tireMakes.find(t => t.id == this.tire.tireMakeId).name, tireModels:this.tireModels});
        this.tireMakeInput.nativeElement.value = this.tireMakes.find(t => t.id == this.tire.tireMakeId).name;
        this.tireModelInput.nativeElement.value = this.tireModels.find(t => t.id == this.tire.tireModelId).name;
        this.tireDimInput.nativeElement.value = this.tireDims.find(t => t.id == this.tire.tireDimId).name;
      }
    }, err => {
        if(err.status == 404)
          this.router.navigate(['/home']); // should go to a route "not-found"
    });
  }

  private setTire(t: Tire){
    this.tire.id = t.id;
    this.tire.tireMakeId = t.tireMake.id;
    this.tire.tireModelId = t.tireModel.id;
    this.tire.tireDimId = t.tireDim.id;
    this.tire.tireType = t.tireType;
    this.tire.contactId = t.contactId;

    this.populateModels();
  }

  // onTireMakeChange() {
  //   this.populateModels();
  //   delete this.tire.tireModelId;
  // }

  private populateModels() {
    var selectedTireMake = this.tireMakes.find(t => t.id == this.tire.tireMakeId);
    this.tireModels = selectedTireMake ? selectedTireMake.tireModels : [];
  }

  filterTireMakes(val: string) {
    this.tiremakeSelected();
    return val ? this.tireMakes.filter(t => new RegExp(`^${val}`, 'gi').test(t.name))
      : this.tireMakes;
  }
  filterTireModels(val: string) {
    this.tire.tireModelId = this.tireModelCtrl.value.id ? this.tireModelCtrl.value.id : 0 ;
    return val ? this.tireModels.filter(t => new RegExp(`^${val}`, 'gi').test(t.name))
      : this.tireModels;
  }
  filterTireDims(val: string) {
    this.tire.tireDimId = this.tireDimCtrl.value.id ? this.tireDimCtrl.value.id : 0 ;
    return val ? this.tireDims.filter(t => new RegExp(`^${val}`, 'gi').test(t.name))
      : this.tireDims;
  }

  displayFn(value: any): string {
    return value && typeof value === 'object' ? value.name : value;
  }

  private tiremakeSelected(){
    this.tire.tireMakeId = this.tireMakeCtrl.value.id ? this.tireMakeCtrl.value.id : 0 ;
    this.tireModels = this.tireMakeCtrl.value.tireModels ? this.tireMakeCtrl.value.tireModels : [] ;
    this.tireModelCtrl = new FormControl({id: 0, name: ''});
    if(this.tireModels != null){
      this.reactiveTireModels = this.tireModelCtrl.valueChanges
        .startWith(this.tireModelCtrl.value)
        .map(val => this.displayFn(val))
        .map(name => this.filterTireModels(name));
    }
  }

  submit() {
    var result$ = (this.tire.id) ? this.tireService.update(this.tire) : this.tireService.create(this.tire);
    result$.subscribe(tire => {
      this.toastyService.success({
        title: 'Success', 
        msg: 'Data was sucessfully saved.',
        theme: 'bootstrap',
        showClose: true,
        timeout: 5000
      });
      this.router.navigate(['/tires/', tire.id])
    });
  }
}
