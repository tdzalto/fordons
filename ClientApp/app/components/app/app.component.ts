import { MediaReplayService } from './../../core/sidenav/mediareplay/media-replay.service';
import { AuthService } from './../../services/auth.service';
import { Component } from '@angular/core';


@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['../../../styles.scss', './app.component.css']
})

export class AppComponent {

    constructor(public authService: AuthService,
        mediaReplayService: MediaReplayService){
        authService.handleAuthentication();
    }
}
