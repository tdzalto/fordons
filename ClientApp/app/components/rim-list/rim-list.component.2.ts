// import { routeAnimation } from './../../route.animation';
// import { Router } from '@angular/router';
// import { AuthService } from './../../services/auth.service';
// import { Rim, KeyValuePair } from './../../models/rim';
// import { RimService } from './../../services/rim.service';
// import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
// import { ResizeSensor } from 'css-element-queries';


// @Component({
//     selector: 'ms-rim-list',
//     templateUrl: 'rim-list.component.html',
//     styleUrls: ['./rim-list.component.scss'],
//     host: {
//       '[@routeAnimation]': 'true'
//     },
//     animations: [ routeAnimation ]
// })

// export class RimListComponent implements OnInit {
//     private readonly PAGE_SIZE = 3;

//     @ViewChild('tbody')
//     tbody: ElementRef;

//     cellWidths = [];
    
//     tableHover: boolean = true;
//     tableStriped: boolean = true;
//     tableCondensed: boolean = true;
//     tableBordered: boolean = true;

//     profile: any;
//     email:"";
//     queryResult: any ={"items":[]};
//     rimDims: KeyValuePair[];
//     query: any = {
//         pageSize: this.PAGE_SIZE
//     };
//     columns = [
//         { title: 'Id' },
//         { title: 'Namn', key: 'name', isSortable: true },
//         { title: 'Dimension', key: 'rimDim.name', isSortable: true },
//         { title: 'Storlek', key: 'size', isSortable: true },
//     ];

//     constructor(private rimService: RimService,
//          private authService: AuthService,
//         private router: Router ) { }

//             renderTableHead() {
//                 console.log(this.tbody.nativeElement)
    
//                 let cells = this.tbody.nativeElement.children[0].children;
            
//                 for (let cell of cells) {
//                   this.cellWidths.push(cell.offsetWidth);
//                 }
            
//                 let resizeSensor = new ResizeSensor(this.tbody.nativeElement, () => {
//                   this.cellWidths.length = 0;
            
//                   for (let cell of cells) {
//                     this.cellWidths.push(cell.offsetWidth);
//                     }
//                     });
//                 }    

//     ngOnInit() {
//         this.rimService.getRimDims()
//             .subscribe(rimDim => this.rimDims = rimDim);

//         this.getProfile();
//     }

//     private getProfile(): Promise<void> {
//         return new Promise((resolve, reject) => {
//             this.authService.getProfile((err, profile) => {
//                 this.profile = profile;
//               });
    
//           if(this.profile != null){
//               this.email = this.profile.email;
//                 resolve();
//           }else{
//             this.authService.delay(1000).then(() =>{
//               if(this.profile != null){
//                 this.email = this.profile.email;
//                 resolve();
//               }else{
//                 reject();
//               }}
//             )
//           } 
//           })
//           .then(() => { 
//                 this.populateRims();
//             },
//             err => {
//               if(err.status == 404)
//                 this.router.navigate(['/home']); // should go to a route "not-found"
//             })
//           .catch(() => { 
//             console.log("Promise to return profile is rejected!");
//           });
//         }

//     private populateRims(){
//         this.rimService.getRims(this.query)
//             .subscribe(result => {
//                  this.queryResult["items"] = result.items.filter(item =>
//                      item.contact.email == this.email)});
//                     };

//     onFilterChange(){
//         this.query.page = 1;
//         this.populateRims();
//     }

//     resetFilter() {
//         this.query = {
//             page: 1,
//             pageSize: this.PAGE_SIZE
//         };
//         this.populateRims();
//     }

//     sortBy(columnName){
//         if(this.query.sortBy === columnName){
//             this.query.isSortAscending = !this.query.isSortAscending;
//         }else{
//             this.query.sortBy = columnName;
//             this.query.isSortAscending = true;
//         }
//         this.populateRims();
//     }

//     onPageChange(page){
//         this.query.page = page;
//         this.populateRims();
//     }
// }