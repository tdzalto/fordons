import { Contact } from './../../models/contact';
import { ContactService } from './../../services/contact.service';
import { Observable } from 'rxjs/Observable';
import { routeAnimation } from './../../route.animation';
import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Rim, KeyValuePair } from './../../models/rim';
import { RimService } from './../../services/rim.service';
import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { ResizeSensor } from 'css-element-queries';
import { merge } from 'rxjs/observable/merge';
import { catchError } from 'rxjs/operators/catchError';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { startWith } from 'rxjs/operators/startWith';
import { switchMap } from 'rxjs/operators/switchMap';
import { map } from 'rxjs/operators/map';
import {of as observableOf} from 'rxjs/observable/of';


@Component({
    selector: 'table-http-example',
    templateUrl: 'rim-list.component.html',
    styleUrls: ['./rim-list.component.scss'],
})

export class RimListComponent implements AfterViewInit {

        displayedColumns = [ 'id', 'lastUpdate', 'name', 'rimDim', 'size', 'type'];
        dataSource = new MatTableDataSource();
        resultsLength = 0;
        isLoadingResults = false;
        isRateLimitReached = false;
        user:any;

        tableHover: boolean = true;
        tableStriped: boolean = true;
        tableCondensed: boolean = true;
        tableBordered: boolean = true;

      
        @ViewChild(MatPaginator) paginator: MatPaginator;
        @ViewChild(MatSort) sort: MatSort;

        constructor(private rimService: RimService,
          private authService: AuthService,
          private router: Router,
          private contactService: ContactService) { }
      
        ngAfterViewInit() {
          // If the user changes the sort order, reset back to the first page.
          this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
      
          merge(this.sort.sortChange, this.paginator.page)
            .pipe(
              startWith({}),
              switchMap(() => {
                this.isLoadingResults = false;
                return this.contactService.getContactByToken(localStorage.getItem('token'));
              }),
              map(data => {
                // Flip flag to show that loading has finished.
                this.isLoadingResults = false;
                this.isRateLimitReached = false;
                //this.resultsLength = data.totalItems;
                return data.rims;
              }),
              catchError(() => {
                this.isLoadingResults = false;
                // Catch if the GitHub API has reached its rate limit. Return empty data.
                this.isRateLimitReached = false;
                return observableOf([]);
              })
            ).subscribe(data => this.dataSource.data = data);
        }
      }
      
      


    
    