import { FormControl } from '@angular/forms';
import { AuthService } from './../../services/auth.service';
import { ContactService } from './../../services/contact.service';
import { BilUppgifterService } from './../../services/biluppgifter.service';
import * as _ from 'underscore';
import { SaveVehicle, Vehicle, CarInfo } from './../../models/vehicle';
import { VehicleService } from './../../services/vehicle.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

@Component({
  selector: 'app-vehicle-form',
  templateUrl: './vehicle-form.component.html',
  styleUrls: ['./vehicle-form.component.scss']
})
export class VehicleFormComponent implements OnInit {
  @ViewChild('makeInput') makeInput: ElementRef;
  @ViewChild('modelInput') modelInput: ElementRef;
  currentMake = '';
  reactiveMakeAuto: any;
  currentModel = '';
  reactiveModelAuto: any;
  makeCtrl: FormControl;
  reactiveMakes: any;
  modelCtrl: FormControl;
  reactiveModels: any;

  
  carInfos: string[] = ['regnr',	'fullname',	'vehicle_year',	'model_year',	'color',	'chassi',	'transmission',	'fuel_1',	'consumption_1',	'power_kw_1',	'power_hp_1',	'co2_1',	'nox_1',	'sound_level_1',	'imported',	'first_registered',	'first_on_swedish_roads',	'purchased',	'number_of_owners',	'meter',	'lastest_inspection',	'inspection_valid_until',	'tax',	'cylinder_volume',	'top_speed',	'four_wheel_drive',	'number_of_passengers',	'length',	'width',	'height',	'kerb_weight',	'total_weight',	'load_weight',	'passenger_airbag'];
  carInfoLabels: string[] = ['Regnr',	'Namn',	'Fordonsår',	'Modellår',	'Färg',	'Kaross',	'Växellåda',	'Drivmedel',	'Förbruking liter/100km',	'Motoreffekt(KW)',	'Motoreffekt(HP)',	'Utsläpp CO2(g/km)',	'Kväveoxider(NOX)',	'Ljudnivå(Db)',	'Importerad',	'Först registrerad',	'Trafik i Sverige',	'Senast köpt',	'Antal Ägare',	'Mätarställning(mil)',	'Senaste besiktning',	'Besiktning giltig tom.',	'Skatt',	'Motorvolym(cm^3)',	'Topp fart(Km/h)',	'Fyrhjulsdriven',	'Antal passagerare',	'Längd(mm)',	'Bredd(mm)',	'Höjd(mm)',	'Tjänstevikt(kg)',	'Totalvikt(kg)',	'Lastvikt(kg)', 'Passagerare airbag'];
  searchRegNr: string;
  searchCarInfo: any[];
  user:any;
  makes: any[];
  models: any[];
  features: any[];
  tireDims: any[];
  vehicle: SaveVehicle = {
    id: 0,
    makeId: 0,
    modelId: 0,
    isRegistered: false,
    features: [],
    contactId:0,
      regnr: '',
      fullname: '',
      vehicle_year: 0,
      model_year: 0,
      color: '',
      chassi: '',
      transmission: '',
      fuel_1: '',
      consumption_1: '',
      power_kw_1: 0,
      power_hp_1: 0,
      co2_1: '',
      nox_1: '',
      sound_level_1: 0,
      imported: '',
      first_registered: '',
      first_on_swedish_roads: '',
      purchased: '',
      number_of_owners: 0,
      meter: 0,
      lastest_inspection: '',
      inspection_valid_until: '',
      tax: 0,
      cylinder_volume: 0,
      top_speed: '',
      four_wheel_drive: '',
      number_of_passengers: 0,
      length: 0,
      width: 0,
      height: 0,
      kerb_weight: 0,
      total_weight: 0,
      load_weight: 0,
      passenger_airbag: '',
  };
 

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private vehicleService: VehicleService,
    private contactService: ContactService,
    private authService: AuthService,
    private BilUppgifterService: BilUppgifterService,
    private toastyService: ToastyService) { 

      route.params.subscribe(p => {
        this.vehicle.id = +p['id'] || 0; //puts + to convert to a number
      })

      this.makeCtrl = new FormControl({id: 0, name: '', models:[]});
      this.reactiveMakes = this.makeCtrl.valueChanges
        .startWith(this.makeCtrl.value)
        .map(val => this.displayFn(val))
        .map(name => this.filterMakes(name));
    }

  ngOnInit() {
    var sources = [
      this.vehicleService.getMakes(),
      this.vehicleService.getFeatures(),
      this.contactService.getContactByToken(localStorage.getItem('token')),
    ];
    
    this.getProfile(this.authService);

    if(this.vehicle.id){
      sources.push(this.vehicleService.getVehicle(this.vehicle.id));
    }

    //Putting all observables in an array and getting them by the index. 
    Observable.forkJoin(sources).subscribe(data => {
      this.makes = data[0];
      this.features = data[1];
      this.vehicle.contactId = data[2].id;

      if(this.vehicle.id){
        this.setVehicle(data[3])

      this.makeInput.nativeElement.value = this.makes.find(v => v.id == this.vehicle.makeId).name;
      this.modelInput.nativeElement.value = this.models.find(v => v.id == this.vehicle.modelId).name;
      }

    }, err => {
        if(err.status == 404)
          this.router.navigate(['/home']); // should go to a route "not-found"
    });
  }

  public getProfile(authService): void {
    this.contactService.getContactByToken(localStorage.getItem('token'))
    .subscribe(x => {
      this.user = x;
      if(this.user != null)
        this.vehicle.contactId = this.user.id;
    },
    err => {
      if(err.status == 404){
        authService.login(); 
        console.log('you session has expired');
      }
    });
}

  private setVehicle(v: Vehicle){
    this.vehicle.id = v.id;
    this.vehicle.contactId = v.contactId;
    this.vehicle.makeId = v.make.id;
    this.vehicle.modelId = v.model.id;
    this.vehicle.isRegistered = v.isRegistered;
    this.vehicle.contactId = v.contactId;
    this.vehicle.features = _.pluck(v.features, 'id');
    
    this.vehicle.regnr = v.regnr;
    this.vehicle.fullname = v.fullname;
    this.vehicle.vehicle_year = v.vehicle_year;
    this.vehicle.model_year = v.model_year;
    this.vehicle.color = v.color;
    this.vehicle.chassi = v.chassi;
    this.vehicle.transmission = v.transmission;
    this.vehicle.fuel_1 = v.fuel_1;
    this.vehicle.consumption_1 = v.consumption_1;
    this.vehicle.power_kw_1 = v.power_kw_1;
    this.vehicle.power_hp_1 = v.power_hp_1;
    this.vehicle.co2_1 = v.co2_1;
    this.vehicle.nox_1 = v.nox_1;
    this.vehicle.sound_level_1 = v.sound_level_1;
    this.vehicle.imported = v.imported;
    this.vehicle.first_registered = v.first_registered;
    this.vehicle.first_on_swedish_roads = v.first_on_swedish_roads;
    this.vehicle.purchased = v.purchased;
    this.vehicle.number_of_owners = v.number_of_owners;
    this.vehicle.meter = v.meter;
    this.vehicle.lastest_inspection = v.lastest_inspection;
    this.vehicle.inspection_valid_until = v.inspection_valid_until;
    this.vehicle.tax = v.tax;
    this.vehicle.cylinder_volume = v.cylinder_volume;
    this.vehicle.top_speed = v.top_speed;
    this.vehicle.four_wheel_drive = v.four_wheel_drive;
    this.vehicle.number_of_passengers = v.number_of_passengers;
    this.vehicle.length = v.length;
    this.vehicle.width = v.width;
    this.vehicle.height = v.height;
    this.vehicle.kerb_weight = v.kerb_weight;
    this.vehicle.total_weight = v.total_weight;
    this.vehicle.load_weight = v.load_weight;
    this.vehicle.passenger_airbag = v.passenger_airbag;

    //get the models after make is known
    this.populateModels();
  }

  onMakeChange() {
    this.populateModels();
    delete this.vehicle.modelId;
  }

  private populateModels() {
    var selectedMake = this.makes.find(m => m.id == this.vehicle.makeId);
    console.log(this.vehicle.makeId);
    this.models = selectedMake ? selectedMake.models : [];
  }

  filterMakes(val: string) {
    this.makeSelected();
    return val ? this.makes.filter(v => new RegExp(`^${val}`, 'gi').test(v.name))
      : this.makes;
  }
  filterModels(val: string) {
    this.vehicle.modelId = this.modelCtrl.value.id ? this.modelCtrl.value.id : 0 ;
    return val ? this.models.filter(v => new RegExp(`^${val}`, 'gi').test(v.name))
      : this.models;
  }

  displayFn(value: any): string {
    return value && typeof value === 'object' ? value.name : value;
  }

  private makeSelected(){
    this.vehicle.makeId = this.makeCtrl.value.id ? this.makeCtrl.value.id : 0 ;
    this.models = this.makeCtrl.value.models ? this.makeCtrl.value.models : [] ;
    this.modelCtrl = new FormControl({id: 0, name: ''});
    if(this.models != null){
      this.reactiveModels = this.modelCtrl.valueChanges
        .startWith(this.modelCtrl.value)
        .map(val => this.displayFn(val))
        .map(name => this.filterModels(name));
    }
  }

  onFeatureToggle(featureId, $event) {
    if ($event.target.checked)
      this.vehicle.features.push(featureId);
    else{
      var index = this.vehicle.features.indexOf(featureId);
      this.vehicle.features.splice(index, 1);
    }  
  }

  getCarInfo(searchRegNr){
    var result = this.BilUppgifterService.getCarInfo(searchRegNr)
    .subscribe(data => {
      this.searchCarInfo = data;
      this.vehicle.regnr = this.searchCarInfo['regnr'];
      this.vehicle.fullname = this.searchCarInfo['fullname'];
      this.vehicle.vehicle_year = this.searchCarInfo['vehicle_year'];
      this.vehicle.model_year = this.searchCarInfo['model_year'];
      this.vehicle.color = this.searchCarInfo['color'];
      this.vehicle.chassi = this.searchCarInfo['chassi'];
      this.vehicle.transmission = (this.searchCarInfo['transmission'] == null )? '-' : (this.searchCarInfo['transmission'] == 1 )? 'MANUELL' : 'AUTOMAT';
      this.vehicle.fuel_1 = (this.searchCarInfo['fuel_1']== null )? '-' : (this.searchCarInfo['fuel_1'] == 1 )? 'BENSIN' : (this.searchCarInfo['fuel_1'] == 2 )? 'DIESEL': 'ETANOL';
      this.vehicle.consumption_1 = this.searchCarInfo['consumption_1'];
      this.vehicle.power_kw_1 = (this.searchCarInfo['power_kw_1']==null)?0:this.searchCarInfo['power_kw_1'];
      this.vehicle.power_hp_1 = (this.searchCarInfo['power_hp_1']==null)?0:this.searchCarInfo['power_hp_1'];
      this.vehicle.co2_1 = this.searchCarInfo['co2_1'];
      this.vehicle.nox_1 = this.searchCarInfo['nox_1'];
      this.vehicle.sound_level_1 = (this.searchCarInfo['sound_level_1']==null)?0:this.searchCarInfo['sound_level_1'];
      this.vehicle.imported = this.searchCarInfo['imported'] ? 'JA' : 'Nej';
      this.vehicle.first_registered = this.searchCarInfo['first_registered'];
      this.vehicle.first_on_swedish_roads = this.searchCarInfo['first_on_swedish_roads'];
      this.vehicle.purchased = this.searchCarInfo['purchased'];
      this.vehicle.number_of_owners = (this.searchCarInfo['number_of_owners']==null)?0:this.searchCarInfo['number_of_owners'];
      this.vehicle.meter = (this.searchCarInfo['meter']==null)?0:this.searchCarInfo['meter'];
      this.vehicle.lastest_inspection = this.searchCarInfo['lastest_inspection'];
      this.vehicle.inspection_valid_until = this.searchCarInfo['inspection_valid_until'];
      this.vehicle.tax = (this.searchCarInfo['tax']==null) ? 0 : this.searchCarInfo['tax'];
      this.vehicle.cylinder_volume = (this.searchCarInfo['cylinder_volume']==null) ? 0 : this.searchCarInfo['cylinder_volume'];
      this.vehicle.top_speed = this.searchCarInfo['top_speed'];
      this.vehicle.four_wheel_drive = this.searchCarInfo['four_wheel_drive'] ? 'JA' : 'Nej';
      this.vehicle.number_of_passengers = (this.searchCarInfo['number_of_passengers']==null)?0:this.searchCarInfo['number_of_passengers'];
      this.vehicle.length = (this.searchCarInfo['length']==null) ? 0 : this.searchCarInfo['length'];
      this.vehicle.width = (this.searchCarInfo['width']==null) ? 0 : this.searchCarInfo['width'];
      this.vehicle.height = (this.searchCarInfo['height']==null) ? 0 : this.searchCarInfo['height'];
      this.vehicle.kerb_weight = (this.searchCarInfo['kerb_weight']==null) ? 0: this.searchCarInfo['kerb_weight'];
      this.vehicle.total_weight = (this.searchCarInfo['total_weight']==null) ? 0: this.searchCarInfo['total_weight'];
      this.vehicle.load_weight = (this.searchCarInfo['load_weight']==null)? 0: this.searchCarInfo['load_weight'];
      this.vehicle.passenger_airbag = this.searchCarInfo['passenger_airbag'] ? 'JA' : 'Nej';
    })
  }

  submit() {
    var result$ = (this.vehicle.id) ? this.vehicleService.update(this.vehicle) : this.vehicleService.create(this.vehicle); 
    result$.subscribe(vehicle => {
      this.toastyService.success({
        title: 'Success', 
        msg: 'Data was sucessfully saved.',
        theme: 'bootstrap',
        showClose: true,
        timeout: 5000
      });
      this.router.navigate(['/vehicles/', vehicle.id])
    });
  }
}
