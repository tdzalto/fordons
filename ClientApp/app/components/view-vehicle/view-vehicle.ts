import { AuthService } from './../../services/auth.service';
import { BrowserXhr } from '@angular/http';
import { ProgressService, BrowserXhrWithProgress } from './../../services/progress.service';
import { PhotoService } from './../../services/photo.service';
import { ToastyService } from 'ng2-toasty';
import { VehicleService } from './../../services/vehicle.service';
import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'underscore';

@Component({
  templateUrl: 'view-vehicle.html',
  providers: [
    { provide: BrowserXhr, useClass: BrowserXhrWithProgress },
    ProgressService
  ]
})
export class ViewVehicleComponent implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef; // viewChild is the template variable #fileInput  
  vehicle: any;
  vehicleId: number; 
  photos: any[];
  progress: any;

  constructor(
    private auth: AuthService,
    private zone: NgZone, // needs to run in angular zone
    private route: ActivatedRoute, 
    private router: Router,
    private toasty: ToastyService,
    private progressService: ProgressService,
    private photoService: PhotoService, // add the service to the component (10)
    private vehicleService: VehicleService) { 

    route.params.subscribe(p => {
      this.vehicleId = +p['id'];
      if (isNaN(this.vehicleId) || this.vehicleId <= 0) {
        router.navigate(['/vehicles']);
        return; 
      }
    });
  }

  ngOnInit() {
    this.photoService.getVehiclePhotos(this.vehicleId) 
      .subscribe(photos => {this.photos = photos
      });
    
    

    this.vehicleService.getVehicle(this.vehicleId)
      .subscribe(
        v => this.vehicle = v,
        err => {
          if (err.status == 404) {
            this.router.navigate(['/vehicles']);
            return; 
          }
        });
  }

  delete() {
    if (confirm("Are you sure?")) {
      this.vehicleService.delete(this.vehicle.id)
        .subscribe(x => {
          this.router.navigate(['/vehicles']);
        });
    }
  }

  uploadPhoto() {    
    this.progressService.startTracking()
      .subscribe(progress => {
        this.zone.run(() => {
          this.progress = progress;
        });
      },
      null,
      () => { this.progress = null; });

    var nativeElement: HTMLInputElement = this.fileInput.nativeElement; //reference to fileInput
    var file = nativeElement.files[0];
    nativeElement.value = ''; 
    this.photoService.uploadVehiclePhoto(this.vehicleId, file)
      .subscribe(photo => {
        this.photos.push(photo);
      },
      err => {
        this.toasty.error({
          title: 'Error',
          msg: err.text(),
          theme: 'bootstrap',
          showClose: true,
          timeout: 5000
        });
      });
  }

  deletePhoto(id) {
    this.progressService.startTracking()
    .subscribe(progress => {
      this.zone.run(() => {
        this.progress = progress;
      });
    },
    null,
    () => { this.progress = null; });

  this.photoService.deleteVehiclePhoto(this.vehicleId, id)
    .subscribe(photo => {
      var index = this.photos.map(function(x) {return x.id; }).indexOf(id);
      this.photos.splice(index, 1);
    },
    err => {
      this.toasty.error({
        title: 'Error',
        msg: err.text(),
        theme: 'bootstrap',
        showClose: true,
        timeout: 5000
      });
    });
  }
}