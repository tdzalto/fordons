import { AuthService } from './../../services/auth.service';
import { BrowserXhr } from '@angular/http';
import { ProgressService, BrowserXhrWithProgress } from './../../services/progress.service';
import { PhotoService } from './../../services/photo.service';
import { ToastyService } from 'ng2-toasty';
import { RimService } from './../../services/rim.service';
import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'underscore';

@Component({
  templateUrl: 'view-rim.html',
  providers: [
    { provide: BrowserXhr, useClass: BrowserXhrWithProgress },
    ProgressService
  ]
})
export class ViewRimComponent implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef; // viewChild is the template variable #fileInput  
  rim: any;
  rimId: number; 
  photos: any[];
  progress: any;

  constructor(
    private auth: AuthService,
    private zone: NgZone, // needs to run in angular zone
    private route: ActivatedRoute, 
    private router: Router,
    private toasty: ToastyService,
    private progressService: ProgressService,
    private photoService: PhotoService, // add the service to the component (10)
    private rimService: RimService) { 

    route.params.subscribe(p => {
      this.rimId = +p['id'];
      if (isNaN(this.rimId) || this.rimId <= 0) {
        router.navigate(['/rims']);
        return; 
      }
    });
  }

  ngOnInit() {
    this.photoService.getRimPhotos(this.rimId) 
      .subscribe(photos => {this.photos = photos
      });
    
    

    this.rimService.getRim(this.rimId)
      .subscribe(
        v => this.rim = v,
        err => {
          if (err.status == 404) {
            this.router.navigate(['/rims']);
            return; 
          }
        });
  }

  delete() {
    if (confirm("Are you sure?")) {
      this.rimService.delete(this.rim.id)
        .subscribe(x => {
          this.router.navigate(['/rims']);
        });
    }
  }

  uploadPhoto() {    
    this.progressService.startTracking()
      .subscribe(progress => {
        this.zone.run(() => {
          this.progress = progress;
        });
      },
      null,
      () => { this.progress = null; });

    var nativeElement: HTMLInputElement = this.fileInput.nativeElement; //reference to fileInput
    var file = nativeElement.files[0];
    nativeElement.value = ''; 
    this.photoService.uploadRimPhoto(this.rimId, file)
      .subscribe(photo => {
        this.photos.push(photo);
      },
      err => {
        this.toasty.error({
          title: 'Error',
          msg: err.text(),
          theme: 'bootstrap',
          showClose: true,
          timeout: 5000
        });
      });
  }

  deletePhoto(id) {
    this.progressService.startTracking()
    .subscribe(progress => {
      this.zone.run(() => {
        this.progress = progress;
      });
    },
    null,
    () => { this.progress = null; });

  this.photoService.deleteRimPhoto(this.rimId, id)
    .subscribe(photo => {
      var index = this.photos.map(function(x) {return x.id; }).indexOf(id);
      this.photos.splice(index, 1);
    },
    err => {
      this.toasty.error({
        title: 'Error',
        msg: err.text(),
        theme: 'bootstrap',
        showClose: true,
        timeout: 5000
      });
    });
  }
}