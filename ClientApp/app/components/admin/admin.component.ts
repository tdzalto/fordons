import { CsvService } from './../../services/csv.service';
import { BrowserXhr } from '@angular/http';
import { ProgressService, BrowserXhrWithProgress } from './../../services/progress.service';
import { ToastyService } from 'ng2-toasty';
import { ActivatedRoute, Router } from '@angular/router';
import { NgZone, ViewChild, ElementRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
    templateUrl: 'view-admin.html',
    providers: [
        { provide: BrowserXhr, useClass: BrowserXhrWithProgress },
        ProgressService
      ]
})

export class AdminPageComponent implements OnInit {
    @ViewChild('fileInputTireDims') fileInputTireDims: ElementRef;
    @ViewChild('fileInputModels') fileInputModels: ElementRef;
    @ViewChild('fileInputMakes') fileInputMakes: ElementRef;
    @ViewChild('fileInputRimDims') fileInputRimDims: ElementRef;
    progress: any;
    csvs: any[];
    data = {
        labels: ['BMW', 'Audi', 'Mazda'],
        datasets: [
            {
                data: [5, 3, 1],
                backgroundColor: [
                    "#ff6384",
                    "#36a2eb",
                    "#ffce56"
                ]   
            }
        ]
    }

    constructor(
        private zone: NgZone, // needs to run in angular zone
        private route: ActivatedRoute, 
        private router: Router,
        private toasty: ToastyService,
        private progressService: ProgressService,
        private csvService: CsvService)
         { 
    }

    ngOnInit() { }

    uploadCsv(type) {    
        this.progressService.startTracking()
          .subscribe(progress => {
            this.zone.run(() => {
              this.progress = progress;
            });
          },
          null,
          () => { this.progress = null; });
        if(this.fileInputTireDims.nativeElement.value != '')
            var nativeElement: HTMLInputElement = this.fileInputTireDims.nativeElement; //reference to fileInput
        if(this.fileInputModels.nativeElement.value != '')
            var nativeElement: HTMLInputElement = this.fileInputModels.nativeElement; //reference to fileInput
        if(this.fileInputMakes.nativeElement.value != '')
            var nativeElement: HTMLInputElement = this.fileInputMakes.nativeElement; //reference to fileInput
        if(this.fileInputRimDims.nativeElement.value != '')    
            var nativeElement: HTMLInputElement = this.fileInputRimDims.nativeElement; //reference to fileInput
        var file = nativeElement.files[0];
        console.log(nativeElement.files);
        nativeElement.value = '';
        this.csvService.upload(file, type)
        .subscribe(file => {
            this.csvs.push(file);
          }),
          err => {
            console.log(err)
            this.toasty.error({
              title: 'Error',
              msg: err.text(),
              theme: 'bootstrap',
              showClose: true,
              timeout: 5000
            });
          };
      }
}