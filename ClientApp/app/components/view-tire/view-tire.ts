import { TireService } from './../../services/tire.service';
import { AuthService } from './../../services/auth.service';
import { BrowserXhr } from '@angular/http';
import { ProgressService, BrowserXhrWithProgress } from './../../services/progress.service';
import { PhotoService } from './../../services/photo.service';
import { ToastyService } from 'ng2-toasty';
import { RimService } from './../../services/rim.service';
import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'underscore';

@Component({
  templateUrl: 'view-tire.html',
  providers: [
    { provide: BrowserXhr, useClass: BrowserXhrWithProgress },
    ProgressService
  ]
})
export class ViewTireComponent implements OnInit {
  @ViewChild('fileInput') fileInput: ElementRef; // viewChild is the template variable #fileInput  
  tire: any;
  tireId: number; 
  photos: any[];
  progress: any;

  constructor(
    private auth: AuthService,
    private zone: NgZone, // needs to run in angular zone
    private route: ActivatedRoute, 
    private router: Router,
    private toasty: ToastyService,
    private progressService: ProgressService,
    private photoService: PhotoService, // add the service to the component (10)
    private tireService: TireService) { 

    route.params.subscribe(p => {
      this.tireId = +p['id'];
      if (isNaN(this.tireId) || this.tireId <= 0) {
        router.navigate(['/tires']);
        return; 
      }
    });
  }

  ngOnInit() {
    this.photoService.getRimPhotos(this.tireId) 
      .subscribe(photos => {this.photos = photos
      });
    
    

    this.tireService.getTire(this.tireId)
      .subscribe(
        v => this.tire = v,
        err => {
          if (err.status == 404) {
            this.router.navigate(['/tires']);
            return; 
          }
        });
  }

  delete() {
    if (confirm("Are you sure?")) {
      this.tireService.delete(this.tire.id)
        .subscribe(x => {
          this.router.navigate(['/tires']);
        });
    }
  }

  uploadPhoto() {    
    this.progressService.startTracking()
      .subscribe(progress => {
        this.zone.run(() => {
          this.progress = progress;
        });
      },
      null,
      () => { this.progress = null; });

    var nativeElement: HTMLInputElement = this.fileInput.nativeElement; //reference to fileInput
    var file = nativeElement.files[0];
    nativeElement.value = ''; 
    this.photoService.uploadTirePhoto(this.tireId, file)
      .subscribe(photo => {
        this.photos.push(photo);
      },
      err => {
        this.toasty.error({
          title: 'Error',
          msg: err.text(),
          theme: 'bootstrap',
          showClose: true,
          timeout: 5000
        });
      });
  }

  deletePhoto(id) {
    this.progressService.startTracking()
    .subscribe(progress => {
      this.zone.run(() => {
        this.progress = progress;
      });
    },
    null,
    () => { this.progress = null; });

  this.photoService.deleteTirePhoto(this.tireId, id)
    .subscribe(photo => {
      var index = this.photos.map(function(x) {return x.id; }).indexOf(id);
      this.photos.splice(index, 1);
    },
    err => {
      this.toasty.error({
        title: 'Error',
        msg: err.text(),
        theme: 'bootstrap',
        showClose: true,
        timeout: 5000
      });
    });
  }
}