import { FormControl } from '@angular/forms';
import { AuthService } from './../../services/auth.service';
import { ContactService } from './../../services/contact.service';
import * as _ from 'underscore';
import { SaveRim, Rim } from './../../models/rim';
import { RimService } from './../../services/rim.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import { race } from 'rxjs/operators/race';

@Component({
  selector: 'app-rim-form',
  templateUrl: './rim-form.component.html',
  styleUrls: ['./rim-form.component.scss']
})
export class RimFormComponent implements OnInit {
  @ViewChild('rimSizeInput') rimSizeInput: ElementRef;
  @ViewChild('rimDimInput') rimDimInput: ElementRef;
  currentRimSize = '';
  reactiveRimSizeAuto: any;
  currentRimDim = '';
  reactiveRimDimAuto: any;
  rimSizeCtrl: FormControl;
  reactiveRimSizes: any;
  rimDimCtrl: FormControl;
  reactiveRimDims: any;

  email:"";
  rimDims: any[];
  rimSizes: number[] = [12,13,14,15,16,17,18,19,20,21,22,23,24];
  rim: SaveRim = {
    id: 0,
    name: "",
    rimSize:0,
    rimType: "",
    rimDimId: 0,
    contactId: 0
  };
 

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private contactService: ContactService,
    private authService: AuthService,
    private rimService: RimService,
    private toastyService: ToastyService) { 

      route.params.subscribe(p => {
        this.rim.id = +p['id'] || 0; //puts + to convert to a number
      })

      this.rimSizeCtrl = new FormControl();
      this.reactiveRimSizes = this.rimSizeCtrl.valueChanges
        .startWith(this.rimSizeCtrl.value)
        .map(val => this.displayFn(val))
        .map(name => this.filterRimSizes(name));

      this.rimDimCtrl = new FormControl({id: 0, name:''});
      this.reactiveRimDims = this.rimDimCtrl.valueChanges
        .startWith(this.rimDimCtrl.value)
        .map(val => this.displayFn(val))
        .map(name => this.filterRimDims(name)); 
    }

  ngOnInit() {
    var sources = [
      //add sources
      this.rimService.getRimDims(),
      this.contactService.getContactByToken(localStorage.getItem('token'))
    ];

    if (this.authService.profile) {
      this.email = this.authService.profile.email;
    } else {
    this.authService.getProfile((err, profile) => {
      this.email = profile.email;
    });
  }
  

    if(this.rim.id){
      sources.push(this.rimService.getRim(this.rim.id));
    }

    //Putting all observables in an array and getting them by the index. 
    Observable.forkJoin(sources).subscribe(data => {
      this.rimDims = data[0].items;
      this.rim.contactId = data[1].id;
      
      if(this.rim.id){
        this.setRim(data[2]);

        this.rimDimInput.nativeElement.value = this.rimDims.find(r => r.id == this.rim.rimDimId).name;
        this.rimSizeInput.nativeElement.value = this.rimSizes.find(r => r == this.rim.rimSize);
      }
    }, err => {
        if(err.status == 404)
          this.router.navigate(['/home']); // should go to a route "not-found"
    });
  }

  private setRim(r: Rim){
    this.rim.id = r.id;
    this.rim.name = r.name;
    this.rim.rimDimId = r.rimDim.id;
    this.rim.rimSize = r.rimSize;
    this.rim.rimType = r.rimType;
    this.rim.contactId = r.contactId;
  }

  filterRimSizes(val: string) {
    this.rim.rimSize = this.rimSizeCtrl.value ? this.rimSizeCtrl.value : 0 ;
    return val ? this.rimSizes.filter(r => new RegExp(`^${val}`, 'gi'))
      : this.rimSizes;
  }
  filterRimDims(val: string) {
    this.rim.rimDimId = this.rimDimCtrl.value.id ? this.rimDimCtrl.value.id : 0 ;
    return val ? this.rimDims.filter(t => new RegExp(`^${val}`, 'gi').test(t.name))
      : this.rimDims;
  }
  displayFn(value: any): string {
    return value && typeof value === 'object' ? value.name : value;
  }

  submit() {
    var result$ = (this.rim.id) ? this.rimService.update(this.rim) : this.rimService.create(this.rim); 
    result$.subscribe(rim => {
      this.toastyService.success({
        title: 'Success', 
        msg: 'Data was sucessfully saved.',
        theme: 'bootstrap',
        showClose: true,
        timeout: 5000
      });
      this.router.navigate(['/rims/', rim.id])
    });
  }
}
