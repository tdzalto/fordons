import { RequestOptions, Http } from '@angular/http';
import { TireListComponent } from './components/tire-list/tire-list.component';
import { MatIconRegistry } from '@angular/material';
import { AdminPageComponent } from './components/admin/admin.component';
import { ContactService } from './services/contact.service';
import { VehicleService } from './services/vehicle.service';
import { BilUppgifterService } from './services/biluppgifter.service';
import { PhotoService } from './services/photo.service';
import { RimService } from './services/rim.service';
import { TireService } from './services/tire.service';
import { CsvService } from './services/csv.service';
import { AUTH_PROVIDERS, AuthHttp } from 'angular2-jwt';
import { AuthService } from './services/auth.service';
import { AppErrorHandler } from './app.error-handler';
import { AdminAuthGuardService } from './services/admin-auth-guard.service';
import { HomeComponent } from './components/home/home.component';
import { AdminComponent } from './core/admin/admin.component';
import { TireFormComponent } from './components/tire-form/tire-form.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { ViewRimComponent } from './components/view-rim/view-rim';
import { RimListComponent } from './components/rim-list/rim-list.component';
import { VehicleListComponent } from './components/vehicle-list/vehicle-list.component';
import { ViewVehicleComponent } from './components/view-vehicle/view-vehicle';
import { ViewTireComponent } from './components/view-tire/view-tire';
import { RimFormComponent } from './components/rim-form/rim-form.component';
import { AuthGuardService } from './services/auth-guard.service';
import { VehicleFormComponent } from './components/vehicle-form/vehicle-form.component';
import { NgModule, ErrorHandler } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MediaReplayService } from './core/sidenav/mediareplay/media-replay.service';
import { authHttpServiceFactory } from './factories/authFactory';


const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        children: [
            { path: '', redirectTo: '/home', pathMatch: 'full' },
            { path: 'vehicles/new', component: VehicleFormComponent},
            { path: 'vehicles/edit/:id', component: VehicleFormComponent },
            { path: 'rims/new', component: RimFormComponent, canActivate: [ AuthGuardService ] },
            { path: 'rims/edit/:id', component: RimFormComponent, canActivate: [ AuthGuardService ] },
            { path: 'vehicles/:id', component: ViewVehicleComponent },
            { path: 'vehicles', component: VehicleListComponent },
            { path: 'rims', component: RimListComponent },
            { path: 'tires', component: TireListComponent },
            { path: 'rims/:id', component: ViewRimComponent },
            { path: 'contact', component: ContactFormComponent, canActivate: [ AuthGuardService ] },
            { path: 'tires/new', component: TireFormComponent },
            { path: 'tires/edit/:id', component: TireFormComponent, canActivate: [ AuthGuardService ] },
            { path: 'tires/:id', component: ViewTireComponent },
            //{ path: 'model/new', component: ModelFormComponent, canActivate: [ AdminAuthGuardService ] },
            { path: 'admin', component: AdminPageComponent },
            { path: 'home', component: HomeComponent },
            { path: '**', redirectTo: 'home' },
        ]
    }
]


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [
        RouterModule,
    ],
    providers: [
        MatIconRegistry,
        { provide: ErrorHandler, useClass: AppErrorHandler },
        AuthService,
        { provide: AuthHttp, useFactory: authHttpServiceFactory, deps: [Http, RequestOptions] },
        AuthGuardService,
        AdminAuthGuardService,
        CsvService,
        TireService,
        RimService,
        VehicleService,
        PhotoService,
        BilUppgifterService,
        ContactService,
        MediaReplayService,

    ]
  })
  export class RoutingModule { }

  export const RoutingComponents = [      
    AdminComponent,
    HomeComponent,
    VehicleFormComponent,
    VehicleListComponent,
    ViewVehicleComponent,
    RimFormComponent,
    RimListComponent,
    ViewRimComponent,
    ViewTireComponent,
    TireFormComponent,
    TireListComponent,
    ContactFormComponent,
    AdminPageComponent
];