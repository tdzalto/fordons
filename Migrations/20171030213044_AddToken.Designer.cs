﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;
using vega.Persistance;

namespace vega.Migrations
{
    [DbContext(typeof(VegaDbContext))]
    [Migration("20171030213044_AddToken")]
    partial class AddToken
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("vega.Core.Models.CarSpec", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("VehicleId");

                    b.Property<string>("chassi");

                    b.Property<string>("co2_1");

                    b.Property<string>("color");

                    b.Property<string>("consumption_1");

                    b.Property<int>("cylinder_volume");

                    b.Property<string>("first_on_swedish_roads");

                    b.Property<string>("first_registered");

                    b.Property<bool>("four_wheel_drive");

                    b.Property<string>("fuel_1");

                    b.Property<string>("fullname");

                    b.Property<int>("height");

                    b.Property<bool>("imported");

                    b.Property<string>("inspection_valid_until");

                    b.Property<int>("kerb_weight");

                    b.Property<string>("lastest_inspection");

                    b.Property<int>("length");

                    b.Property<int>("load_weight");

                    b.Property<string>("maker");

                    b.Property<int>("meter");

                    b.Property<string>("model");

                    b.Property<int>("model_year");

                    b.Property<string>("nox_1");

                    b.Property<int>("number_of_owners");

                    b.Property<int>("number_of_passengers");

                    b.Property<bool>("passenger_airbag");

                    b.Property<int>("power_hp_1");

                    b.Property<int>("power_kw_1");

                    b.Property<string>("pre_registered");

                    b.Property<string>("purchased");

                    b.Property<string>("regnr");

                    b.Property<int>("sound_level_1");

                    b.Property<int>("tax");

                    b.Property<string>("top_speed");

                    b.Property<int>("total_weight");

                    b.Property<string>("transmission");

                    b.Property<int>("vehicle_year");

                    b.Property<int>("width");

                    b.HasKey("Id");

                    b.HasIndex("VehicleId")
                        .IsUnique();

                    b.ToTable("CarSpecs");
                });

            modelBuilder.Entity("vega.Core.Models.Contact", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("City")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(5000);

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<DateTime>("LastUpdate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("Phone")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("PostalCode")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("token")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("Contacts");
                });

            modelBuilder.Entity("vega.Core.Models.Feature", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("Features");
                });

            modelBuilder.Entity("vega.Core.Models.Make", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("Makes");
                });

            modelBuilder.Entity("vega.Core.Models.Model", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("MakeId");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.HasIndex("MakeId");

                    b.ToTable("Models");
                });

            modelBuilder.Entity("vega.Core.Models.ModelRimDim", b =>
                {
                    b.Property<int>("ModelId");

                    b.Property<int>("RimDimId");

                    b.HasKey("ModelId", "RimDimId");

                    b.HasIndex("RimDimId");

                    b.ToTable("ModelRimDims");
                });

            modelBuilder.Entity("vega.Core.Models.ModelTireDim", b =>
                {
                    b.Property<int>("ModelId");

                    b.Property<int>("TireDimId");

                    b.HasKey("ModelId", "TireDimId");

                    b.HasIndex("TireDimId");

                    b.ToTable("ModelTireDims");
                });

            modelBuilder.Entity("vega.Core.Models.Photo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FileName")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("VehicleId");

                    b.HasKey("Id");

                    b.HasIndex("VehicleId");

                    b.ToTable("Photos");
                });

            modelBuilder.Entity("vega.Core.Models.Rim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ContactId");

                    b.Property<DateTime>("LastUpdate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("RimDimId");

                    b.Property<int>("RimSize");

                    b.Property<string>("RimType");

                    b.HasKey("Id");

                    b.HasIndex("ContactId");

                    b.HasIndex("RimDimId");

                    b.ToTable("Rims");
                });

            modelBuilder.Entity("vega.Core.Models.RimDim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BoltCircle")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("BoltNutDim")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("Hub")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<string>("Offset")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("RimDims");
                });

            modelBuilder.Entity("vega.Core.Models.Tire", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ContactId");

                    b.Property<DateTime>("LastUpdate");

                    b.Property<int>("TireDimId");

                    b.Property<int>("TireModelId");

                    b.Property<string>("TireType");

                    b.HasKey("Id");

                    b.HasIndex("ContactId");

                    b.HasIndex("TireDimId");

                    b.HasIndex("TireModelId");

                    b.ToTable("Tires");
                });

            modelBuilder.Entity("vega.Core.Models.TireDim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Height");

                    b.Property<string>("Size");

                    b.Property<string>("Width");

                    b.HasKey("Id");

                    b.ToTable("TireDims");
                });

            modelBuilder.Entity("vega.Core.Models.TireMake", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.HasKey("Id");

                    b.ToTable("TireMakes");
                });

            modelBuilder.Entity("vega.Core.Models.TireModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255);

                    b.Property<int>("TireMakeId");

                    b.HasKey("Id");

                    b.HasIndex("TireMakeId");

                    b.ToTable("TireModels");
                });

            modelBuilder.Entity("vega.Core.Models.Vehicle", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ContactId");

                    b.Property<bool>("IsRegistered")
                        .HasMaxLength(255);

                    b.Property<DateTime>("LastUpdate");

                    b.Property<int>("ModelId");

                    b.HasKey("Id");

                    b.HasIndex("ContactId");

                    b.HasIndex("ModelId");

                    b.ToTable("Vehicles");
                });

            modelBuilder.Entity("vega.Core.Models.VehicleFeature", b =>
                {
                    b.Property<int>("VehicleId");

                    b.Property<int>("FeatureId");

                    b.HasKey("VehicleId", "FeatureId");

                    b.HasIndex("FeatureId");

                    b.ToTable("VehicleFeatures");
                });

            modelBuilder.Entity("vega.Core.Models.CarSpec", b =>
                {
                    b.HasOne("vega.Core.Models.Vehicle")
                        .WithOne("CarSpecs")
                        .HasForeignKey("vega.Core.Models.CarSpec", "VehicleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("vega.Core.Models.Model", b =>
                {
                    b.HasOne("vega.Core.Models.Make", "Make")
                        .WithMany("Models")
                        .HasForeignKey("MakeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("vega.Core.Models.ModelRimDim", b =>
                {
                    b.HasOne("vega.Core.Models.Model", "Model")
                        .WithMany("RimDims")
                        .HasForeignKey("ModelId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("vega.Core.Models.RimDim", "RimDim")
                        .WithMany()
                        .HasForeignKey("RimDimId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("vega.Core.Models.ModelTireDim", b =>
                {
                    b.HasOne("vega.Core.Models.Model", "Model")
                        .WithMany("TireDims")
                        .HasForeignKey("ModelId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("vega.Core.Models.TireDim", "TireDim")
                        .WithMany("Models")
                        .HasForeignKey("TireDimId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("vega.Core.Models.Photo", b =>
                {
                    b.HasOne("vega.Core.Models.Vehicle")
                        .WithMany("Photos")
                        .HasForeignKey("VehicleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("vega.Core.Models.Rim", b =>
                {
                    b.HasOne("vega.Core.Models.Contact", "Contact")
                        .WithMany("Rims")
                        .HasForeignKey("ContactId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("vega.Core.Models.RimDim", "RimDim")
                        .WithMany("Rims")
                        .HasForeignKey("RimDimId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("vega.Core.Models.Tire", b =>
                {
                    b.HasOne("vega.Core.Models.Contact", "Contact")
                        .WithMany("Tires")
                        .HasForeignKey("ContactId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("vega.Core.Models.TireDim", "TireDim")
                        .WithMany("Tires")
                        .HasForeignKey("TireDimId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("vega.Core.Models.TireModel", "TireModel")
                        .WithMany()
                        .HasForeignKey("TireModelId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("vega.Core.Models.TireModel", b =>
                {
                    b.HasOne("vega.Core.Models.TireMake", "TireMake")
                        .WithMany("TireModels")
                        .HasForeignKey("TireMakeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("vega.Core.Models.Vehicle", b =>
                {
                    b.HasOne("vega.Core.Models.Contact", "Contact")
                        .WithMany("Vehicles")
                        .HasForeignKey("ContactId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("vega.Core.Models.Model", "Model")
                        .WithMany()
                        .HasForeignKey("ModelId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("vega.Core.Models.VehicleFeature", b =>
                {
                    b.HasOne("vega.Core.Models.Feature", "Feature")
                        .WithMany()
                        .HasForeignKey("FeatureId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("vega.Core.Models.Vehicle", "Vehicle")
                        .WithMany("Features")
                        .HasForeignKey("VehicleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
