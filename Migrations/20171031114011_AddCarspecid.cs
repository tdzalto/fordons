﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace vega.Migrations
{
    public partial class AddCarspecid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CarSpecs_Vehicles_VehicleId",
                table: "CarSpecs");

            migrationBuilder.DropIndex(
                name: "IX_CarSpecs_VehicleId",
                table: "CarSpecs");

            migrationBuilder.AddColumn<int>(
                name: "CarSpecsId",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CarSpecsId1",
                table: "Vehicles",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_CarSpecsId1",
                table: "Vehicles",
                column: "CarSpecsId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicles_CarSpecs_CarSpecsId1",
                table: "Vehicles",
                column: "CarSpecsId1",
                principalTable: "CarSpecs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicles_CarSpecs_CarSpecsId1",
                table: "Vehicles");

            migrationBuilder.DropIndex(
                name: "IX_Vehicles_CarSpecsId1",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "CarSpecsId",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "CarSpecsId1",
                table: "Vehicles");

            migrationBuilder.CreateIndex(
                name: "IX_CarSpecs_VehicleId",
                table: "CarSpecs",
                column: "VehicleId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CarSpecs_Vehicles_VehicleId",
                table: "CarSpecs",
                column: "VehicleId",
                principalTable: "Vehicles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
