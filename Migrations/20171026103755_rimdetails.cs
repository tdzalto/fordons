﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace vega.Migrations
{
    public partial class rimdetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rims_RimDims_RimDimId",
                table: "Rims");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Tires");

            migrationBuilder.DropColumn(
                name: "BoltCircle",
                table: "Rims");

            migrationBuilder.DropColumn(
                name: "BoltNutDim",
                table: "Rims");

            migrationBuilder.DropColumn(
                name: "Hub",
                table: "Rims");

            migrationBuilder.DropColumn(
                name: "Offset",
                table: "Rims");

            migrationBuilder.DropColumn(
                name: "Size",
                table: "Rims");

            migrationBuilder.AlterColumn<int>(
                name: "RimDimId",
                table: "Rims",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RimSize",
                table: "Rims",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "RimType",
                table: "Rims",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Rims_RimDims_RimDimId",
                table: "Rims",
                column: "RimDimId",
                principalTable: "RimDims",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rims_RimDims_RimDimId",
                table: "Rims");

            migrationBuilder.DropColumn(
                name: "RimSize",
                table: "Rims");

            migrationBuilder.DropColumn(
                name: "RimType",
                table: "Rims");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Tires",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RimDimId",
                table: "Rims",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "BoltCircle",
                table: "Rims",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BoltNutDim",
                table: "Rims",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Hub",
                table: "Rims",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Offset",
                table: "Rims",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Size",
                table: "Rims",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Rims_RimDims_RimDimId",
                table: "Rims",
                column: "RimDimId",
                principalTable: "RimDims",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
