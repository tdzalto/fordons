﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace vega.Migrations
{
    public partial class Seed2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Sportstolar Läder')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Navigator (GPS)')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Glastaklucka')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Parkeringsvärmare bränsledriven')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Eluppvärmda Spolare')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('City safety autobroms')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Aktiva Xenonstrålkastare')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('P-sensorer fram+bak')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('High Performance Sound')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Bluetooth')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Hastighetsberoende Servo')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Digitalt Kombiinstrument')");

            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('ACC/Klimatanläggning')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('CD-stereo')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Antisladdsystem')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Elhissar fram')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Elhissar bak')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Stolvärme bak')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Stolvärme fram')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Yttertemperaturmätare')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Elinfällbara backspeglar')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('High Performance Sound')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('ABS')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Fjärrstyrt c-lås')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Backkamera')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('AUX')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('USB')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Dimljus')");
            migrationBuilder.Sql("INSERT INTO Features (Name) VALUES ('Krockgardiner')");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
