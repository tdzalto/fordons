﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace vega.Migrations
{
    public partial class vehicledetails3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Tires",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RimDimId",
                table: "Rims",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rims_RimDimId",
                table: "Rims",
                column: "RimDimId");

            migrationBuilder.AddForeignKey(
                name: "FK_Rims_RimDims_RimDimId",
                table: "Rims",
                column: "RimDimId",
                principalTable: "RimDims",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rims_RimDims_RimDimId",
                table: "Rims");

            migrationBuilder.DropIndex(
                name: "IX_Rims_RimDimId",
                table: "Rims");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Tires");

            migrationBuilder.DropColumn(
                name: "RimDimId",
                table: "Rims");
        }
    }
}
