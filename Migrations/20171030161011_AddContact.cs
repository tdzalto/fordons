﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace vega.Migrations
{
    public partial class AddContact : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContactEmail",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "ContactName",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "ContactPhone",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "ContactEmail",
                table: "Tires");

            migrationBuilder.DropColumn(
                name: "ContactName",
                table: "Tires");

            migrationBuilder.DropColumn(
                name: "ContactPhone",
                table: "Tires");

            migrationBuilder.DropColumn(
                name: "ContactEmail",
                table: "Rims");

            migrationBuilder.DropColumn(
                name: "ContactName",
                table: "Rims");

            migrationBuilder.DropColumn(
                name: "ContactPhone",
                table: "Rims");

            migrationBuilder.AddColumn<int>(
                name: "ContactId",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ContactId",
                table: "Tires",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ContactId",
                table: "Rims",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CarSpecs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    VehicleId = table.Column<int>(type: "int", nullable: false),
                    chassi = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    co2_1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    color = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    consumption_1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    cylinder_volume = table.Column<int>(type: "int", nullable: false),
                    first_on_swedish_roads = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    first_registered = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    four_wheel_drive = table.Column<bool>(type: "bit", nullable: false),
                    fuel_1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    fullname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    height = table.Column<int>(type: "int", nullable: false),
                    imported = table.Column<bool>(type: "bit", nullable: false),
                    inspection_valid_until = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    kerb_weight = table.Column<int>(type: "int", nullable: false),
                    lastest_inspection = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    length = table.Column<int>(type: "int", nullable: false),
                    load_weight = table.Column<int>(type: "int", nullable: false),
                    maker = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    meter = table.Column<int>(type: "int", nullable: false),
                    model = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    model_year = table.Column<int>(type: "int", nullable: false),
                    nox_1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    number_of_owners = table.Column<int>(type: "int", nullable: false),
                    number_of_passengers = table.Column<int>(type: "int", nullable: false),
                    passenger_airbag = table.Column<bool>(type: "bit", nullable: false),
                    power_hp_1 = table.Column<int>(type: "int", nullable: false),
                    power_kw_1 = table.Column<int>(type: "int", nullable: false),
                    pre_registered = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    purchased = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    regnr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    sound_level_1 = table.Column<int>(type: "int", nullable: false),
                    tax = table.Column<int>(type: "int", nullable: false),
                    top_speed = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    total_weight = table.Column<int>(type: "int", nullable: false),
                    transmission = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    vehicle_year = table.Column<int>(type: "int", nullable: false),
                    width = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarSpecs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CarSpecs_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    City = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", maxLength: 5000, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    LastUpdate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    PostalCode = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicles_ContactId",
                table: "Vehicles",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Tires_ContactId",
                table: "Tires",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Rims_ContactId",
                table: "Rims",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_CarSpecs_VehicleId",
                table: "CarSpecs",
                column: "VehicleId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Rims_Contacts_ContactId",
                table: "Rims",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tires_Contacts_ContactId",
                table: "Tires",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicles_Contacts_ContactId",
                table: "Vehicles",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rims_Contacts_ContactId",
                table: "Rims");

            migrationBuilder.DropForeignKey(
                name: "FK_Tires_Contacts_ContactId",
                table: "Tires");

            migrationBuilder.DropForeignKey(
                name: "FK_Vehicles_Contacts_ContactId",
                table: "Vehicles");

            migrationBuilder.DropTable(
                name: "CarSpecs");

            migrationBuilder.DropTable(
                name: "Contacts");

            migrationBuilder.DropIndex(
                name: "IX_Vehicles_ContactId",
                table: "Vehicles");

            migrationBuilder.DropIndex(
                name: "IX_Tires_ContactId",
                table: "Tires");

            migrationBuilder.DropIndex(
                name: "IX_Rims_ContactId",
                table: "Rims");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "Tires");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "Rims");

            migrationBuilder.AddColumn<string>(
                name: "ContactEmail",
                table: "Vehicles",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ContactName",
                table: "Vehicles",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ContactPhone",
                table: "Vehicles",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ContactEmail",
                table: "Tires",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ContactName",
                table: "Tires",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPhone",
                table: "Tires",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ContactEmail",
                table: "Rims",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ContactName",
                table: "Rims",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPhone",
                table: "Rims",
                maxLength: 255,
                nullable: false,
                defaultValue: "");
        }
    }
}
