﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace vega.Migrations
{
    public partial class specificstovehicles2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "co2_1",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "consumption_1",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "cylinder_volume",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "first_on_swedish_roads",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "first_registered",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "four_wheel_drive",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "fuel_1",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "height",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "imported",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "inspection_valid_until",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "kerb_weight",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "lastest_inspection",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "length",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "load_weight",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "meter",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "model_year",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "nox_1",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "number_of_owners",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "number_of_passengers",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "passenger_airbag",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "power_hp_1",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "power_kw_1",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "purchased",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "regnr",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "sound_level_1",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "tax",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "top_speed",
                table: "Vehicles",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "total_weight",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "vehicle_year",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "width",
                table: "Vehicles",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "co2_1",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "consumption_1",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "cylinder_volume",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "first_on_swedish_roads",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "first_registered",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "four_wheel_drive",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "fuel_1",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "height",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "imported",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "inspection_valid_until",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "kerb_weight",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "lastest_inspection",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "length",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "load_weight",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "meter",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "model_year",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "nox_1",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "number_of_owners",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "number_of_passengers",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "passenger_airbag",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "power_hp_1",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "power_kw_1",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "purchased",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "regnr",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "sound_level_1",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "tax",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "top_speed",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "total_weight",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "vehicle_year",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "width",
                table: "Vehicles");
        }
    }
}
