using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AutoMapper;
using vega.Controllers.Resources;
using vega.Core.Models;

namespace vega.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domain to API Resource
            CreateMap<Photo, PhotoResource>();
            CreateMap(typeof(QueryResult<>), typeof(QueryResultResource<>));
            CreateMap<Make, MakeResource>();
            CreateMap<TireMake, TireMakeResource>();
            CreateMap<Make, KeyValuePairResource>();
            CreateMap<Model, KeyValuePairResource>();
            CreateMap<TireModel, KeyValuePairResource>();
            CreateMap<TireMake, KeyValuePairResource>();
            CreateMap<Feature, KeyValuePairResource>();
            CreateMap<Contact, ContactResource>()
            .ForMember(cr => cr.Vehicles, opt => opt.MapFrom(c => c.Vehicles.Select(cv => new VehicleResource { Id = cv.Id, Name = cv.Model.Name, LastUpdate = cv.LastUpdate, IsRegistered = cv.IsRegistered } )))
            .ForMember(cr => cr.Tires, opt => opt.MapFrom(c => c.Tires.Select(ct => new TireResource { Id = ct.Id, Name = ct.Name, Dimension = ct.TireDim.Name, LastUpdate = ct.LastUpdate } )))
            .ForMember(cr => cr.Rims, opt => opt.MapFrom(c => c.Rims.Select(cr => new RimResource { Id = cr.Id, Name = cr.Name, Dimension = cr.RimDim.Name, RimType = cr.RimType, RimSize = cr.RimSize, LastUpdate = cr.LastUpdate } )));
            CreateMap<TireDim, TireDimResource>()
                .ForMember(tdr => tdr.Tires, opt => opt.MapFrom(td => td.Tires.Select(t => new KeyValuePairResource{ Id = t.Id, Name = t.Name})));
            CreateMap<TireDim, KeyValuePairResource>();
            CreateMap<Tire, KeyValuePairResource>();
            CreateMap<Rim, KeyValuePairResource>();
            CreateMap<RimDim, RimDimResource>();
            CreateMap<RimDimResource, KeyValuePairResource>();
            CreateMap<Rim, RimResource>()
                .ForMember(rr => rr.Photos, opt => opt.MapFrom(t => t.Photos.Select(p => new PhotoResource { Id = p.Id, FilePath = p.FilePath, ThumbnailPath = p.ThumbnailPath } )))
                .ForMember(rr => rr.Contact, opt => opt.MapFrom(r => new ContactResource { Name = r.Contact.Name, Email = r.Contact.Email, Phone = r.Contact.Phone } ));
            CreateMap<Tire, TireResource>()
                .ForMember(tr => tr.Photos, opt => opt.MapFrom(t => t.Photos.Select(p => new PhotoResource { Id = p.Id, FilePath = p.FilePath, ThumbnailPath = p.ThumbnailPath } )))
                .ForMember(tr => tr.TireMake, opt => opt.MapFrom(t => t.TireModel.TireMake))
                .ForMember(tr => tr.Contact, opt => opt.MapFrom(t => new ContactResource { Name = t.Contact.Name, Email = t.Contact.Email, Phone = t.Contact.Phone } ));
            CreateMap<Model, ModelResource>()
                .ForMember(mr => mr.TireDims, opt => opt.MapFrom(m => m.TireDims.Select(mt => new KeyValuePairResource { Id = mt.TireDim.Id, Name = mt.TireDim.Name } )))
                .ForMember(mr => mr.RimDims, opt => opt.MapFrom(m => m.RimDims.Select(mr => new KeyValuePairResource { Id = mr.RimDim.Id, Name = mr.RimDim.Name } )));    
            CreateMap<Vehicle, SaveVehicleResource>()
                .ForMember(vr => vr.Features, opt => opt.MapFrom(v => v.Features.Select(vf => vf.FeatureId)));
            CreateMap<Vehicle, VehicleResource>()
                .ForMember(vr => vr.Make, opt => opt.MapFrom(v => v.Model.Make))
                .ForMember(vr => vr.Contact, opt => opt.MapFrom(v => new ContactResource { Id = v.Contact.Id, Name = v.Contact.Name, Email = v.Contact.Email, Phone = v.Contact.Phone } ))
                .ForMember(vr => vr.Features, opt => opt.MapFrom(v => v.Features.Select(vf => new KeyValuePairResource { Id = vf.Feature.Id, Name = vf.Feature.Name } )))
                .ForMember(vr => vr.Photos, opt => opt.MapFrom(v => v.Photos.Select(p => new PhotoResource { Id = p.Id, FilePath = p.FilePath, ThumbnailPath = p.ThumbnailPath } )))
                .ForMember(vr => vr.Tires, opt => opt.MapFrom(v => v.Model.TireDims.Select(td => new KeyValuePairObjectResource{ Id = td.TireDim.Id, Name = td.TireDim.Name, Objects = td.TireDim.Tires.Select(t => new KeyValuePairResource { Id = t.Id, Name = t.Name}).ToList()})))
                .ForMember(vr => vr.Rims, opt => opt.MapFrom(v => v.Model.RimDims.Select(mr => new KeyValuePairObjectResource{ Id = mr.RimDim.Id, Name = mr.RimDim.Name, Objects = mr.RimDim.Rims.Select( r => new KeyValuePairResource { Id = r.Id, Name = r.Name }).ToList()})));

            // API Resource to Domain
            CreateMap<VehicleQueryResource, VehicleQuery>();
            CreateMap<KeyValueQueryResource, KeyValueQuery>();
            CreateMap<ModelQueryResource, ModelQuery>();
            CreateMap<TireQueryResource, TireQuery>();
            CreateMap<DimQueryResource, DimQuery>();
            CreateMap<SaveVehicleResource, Vehicle>()
                .ForMember( v => v.Id, opt => opt.Ignore())
                .ForMember(v => v.Features, opt => opt.Ignore())
                .AfterMap((vr, v) => {
                    //Remove old features
                    var removedFeatures = v.Features.Where(f => !vr.Features.Contains(f.FeatureId));
                    foreach (var f in removedFeatures)
                        v.Features.Remove(f);

                    // Add new features
                    var addedFeatures = vr.Features.Where(id => !v.Features.Any(f => f.FeatureId == id)).Select(id => new VehicleFeature{ FeatureId = id }); 
                    foreach (var f in addedFeatures)
                        v.Features.Add(f);       
                });
            CreateMap<SaveTireResource, Tire>()
                .ForMember( t => t.Id, opt => opt.Ignore());
            CreateMap<SaveRimResource, Rim>()
                .ForMember( r => r.Id, opt => opt.Ignore());
            CreateMap<SaveContactResource, Contact>()
                .ForMember( c => c.Id, opt => opt.Ignore());
        }
    }
}