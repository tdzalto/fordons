using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vega.Controllers.Resources;
using vega.Core.Models;
using vega.Core;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace vega.Controllers
{
    [Route("/api/tires")]
    public class TiresController : Controller
    {
        private readonly IMapper mapper;
        private readonly ITireRepository repository;
        private readonly IUnitOfWork unitOfWork;
        public TiresController(IMapper mapper, ITireRepository repository, IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateTire([FromBody] SaveTireResource tireResource)
        {
            if (!ModelState.IsValid){
                return BadRequest(ModelState);
            }

            var tire = mapper.Map<SaveTireResource, Tire>(tireResource);
            tire.LastUpdate = DateTime.Now;

            repository.Add(tire);
            await unitOfWork.CompleteAsync();

            tire = await repository.GetTire(tire.Id);

            var result = mapper.Map<Tire, TireResource>(tire);

            return Ok(result);
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> UpdateTire(int id, [FromBody] SaveTireResource tireResource)
        {
            if (!ModelState.IsValid){
                return BadRequest(ModelState);
            }

            var tire = await repository.GetTire(id);

            if (tire == null) {
             return NotFound();
            }

            mapper.Map<SaveTireResource, Tire>(tireResource, tire);
            tire.LastUpdate = DateTime.Now;

            await unitOfWork.CompleteAsync();

            tire = await repository.GetTire(tire.Id);
            var result = mapper.Map<Tire, TireResource>(tire);

            return Ok(result);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteTire(int id)
        {
            var tire = await repository.GetTire(id, includeRelated: false);

            if (tire == null){
             return NotFound();
            }

            repository.Remove(tire);
            await unitOfWork.CompleteAsync();

            return Ok(tire);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTire(int id)
        {
            var tire = await repository.GetTire(id);

            if (tire == null) {
             return NotFound();
            }

            var tireResource = mapper.Map<Tire, TireResource>(tire);

            return Ok(tireResource);
        }

        [HttpGet]
        public async Task<QueryResultResource<TireResource>> GetTires(TireQueryResource filterResource)
        {
            var filter = mapper.Map<TireQueryResource, TireQuery>(filterResource);
            var queryResult = await repository.GetTires(filter);

            return mapper.Map<QueryResult<Tire>, QueryResultResource<TireResource>>(queryResult);
        }
    }
}