using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using vega.Controllers.Resources;
using vega.Core;
using vega.Core.Models;

namespace vega.Controllers
{
    public class PhotosController : Controller // Create a controller for the domain (5)
    {
        private readonly IHostingEnvironment host;
        private readonly IVehicleRepository vehicleRepository;
        private readonly IMapper mapper;
        private readonly PhotoSettings photoSettings;
        private readonly IPhotoRepository photoRepository;
        private readonly IPhotoService photoService;
        private readonly IRimRepository rimRepository;
        private readonly ITireRepository tireRepository;

        public PhotosController(IHostingEnvironment host, IVehicleRepository vehicleRepository, IRimRepository rimRepository, ITireRepository tireRepository, IPhotoRepository photoRepository, IMapper mapper, IOptionsSnapshot<PhotoSettings> options, IPhotoService photoService)
        {
            this.tireRepository = tireRepository;
            this.rimRepository = rimRepository;
            this.photoService = photoService;
            this.photoRepository = photoRepository;
            this.photoSettings = options.Value;
            this.mapper = mapper;
            this.vehicleRepository = vehicleRepository;
            this.host = host;
        }
        [Route("api/vehicles/{vehicleId}/photos")]
        [HttpGet]
        public async Task<IEnumerable<PhotoResource>> GetVehiclePhotos(int vehicleId)
        {
            var photos = await photoRepository.GetVehiclePhotos(vehicleId);

            return mapper.Map<IEnumerable<Photo>, IEnumerable<PhotoResource>>(photos);
        }
        [Route("api/vehicles/{vehicleId}/photos")]
        [HttpPost]
        public async Task<IActionResult> UploadVehiclePhoto(int vehicleId, IFormFile file)
        {
            var vehicle = await vehicleRepository.GetVehicle(vehicleId, includeRelated: false);
            if (vehicle == null)
            {
                return NotFound();
            }

            //File validation
            if (file == null) return BadRequest("Null fil");
            if (file.Length == 0) return BadRequest("Tom fil");
            if (file.Length == photoSettings.MaxBytes) return BadRequest("Max filstorlek uppnådd");
            if (!photoSettings.IsSupported(file.FileName)) return BadRequest("Fel filtyp");

            //finds the path to wwwroot/uploads
            var uploadsFolderPath = Path.Combine(host.WebRootPath, "uploads");
            var photo = await photoService.UploadPhoto(vehicle, file, uploadsFolderPath);

            return Ok(mapper.Map<VehiclePhoto, PhotoResource>(photo)); // Map the photoresource to photo
        }


        [HttpDelete("api/vehicles/{vehicleId}/photos/{id}")]
        public async Task<IActionResult> DeleteVehiclePhoto(int vehicleId, int id)
        {
            var photo = await photoRepository.GetVehiclePhoto(vehicleId, id);

            if (photo == null)
            {
                return NotFound();
            }

            photoRepository.RemoveVehiclePhoto(photo);
            var folderPath = Path.Combine(host.WebRootPath, "uploads");
            var storageResult = await photoService.RemovePhoto(folderPath + "/" + photo.FilePath, folderPath + "/" + photo.ThumbnailPath);

            return Ok(photo);
        }

        [Route("api/tires/{tireId}/photos")]
        [HttpGet]
        public async Task<IEnumerable<PhotoResource>> GetTirePhotos(int tireId)
        {
            var photos = await photoRepository.GetTirePhotos(tireId);

            return mapper.Map<IEnumerable<Photo>, IEnumerable<PhotoResource>>(photos);
        }
        [Route("api/tires/{tireId}/photos")]
        [HttpPost]
        public async Task<IActionResult> UploadTirePhotos(int tireId, IFormFile file)
        {
            var tire = await tireRepository.GetTire(tireId, includeRelated: false);
            if (tire == null)
            {
                return NotFound();
            }

            //File validation
            if (file == null) return BadRequest("Null fil");
            if (file.Length == 0) return BadRequest("Tom fil");
            if (file.Length == photoSettings.MaxBytes) return BadRequest("Max filstorlek uppnådd");
            if (!photoSettings.IsSupported(file.FileName)) return BadRequest("Fel filtyp");

            //finds the path to wwwroot/uploads
            var uploadsFolderPath = Path.Combine(host.WebRootPath, "uploads");
            var photo = await photoService.UploadPhoto(tire, file, uploadsFolderPath);

            return Ok(mapper.Map<TirePhoto, PhotoResource>(photo)); // Map the photoresource to photo
        }



        [HttpDelete("api/tires/{tireId}/photos/{id}")]
        public async Task<IActionResult> DeleteTirePhoto(int tireId, int id)
        {
            var photo = await photoRepository.GetTirePhoto(tireId, id);

            if (photo == null)
            {
                return NotFound();
            }

            photoRepository.RemoveTirePhoto(photo);
            var folderPath = Path.Combine(host.WebRootPath, "uploads");
            var storageResult = await photoService.RemovePhoto(folderPath + "/" + photo.FilePath, folderPath + "/" + photo.ThumbnailPath);

            return Ok(photo);
        }

        [Route("api/rims/{rimId}/photos")]
        [HttpGet]
        public async Task<IEnumerable<PhotoResource>> GetPhotos(int rimId)
        {
            var photos = await photoRepository.GetRimPhotos(rimId);

            return mapper.Map<IEnumerable<Photo>, IEnumerable<PhotoResource>>(photos);
        }
        [Route("api/rims/{rimId}/photos")]
        [HttpPost]
        public async Task<IActionResult> UploadRimPhoto(int rimId, IFormFile file)
        {
            var rim = await rimRepository.GetRim(rimId, includeRelated: false);
            if (rim == null)
            {
                return NotFound();
            }

            //File validation
            if (file == null) return BadRequest("Null fil");
            if (file.Length == 0) return BadRequest("Tom fil");
            if (file.Length == photoSettings.MaxBytes) return BadRequest("Max filstorlek uppnådd");
            if (!photoSettings.IsSupported(file.FileName)) return BadRequest("Fel filtyp");

            //finds the path to wwwroot/uploads
            var uploadsFolderPath = Path.Combine(host.WebRootPath, "uploads");
            var photo = await photoService.UploadPhoto(rim, file, uploadsFolderPath);

            return Ok(mapper.Map<RimPhoto, PhotoResource>(photo)); // Map the photoresource to photo
        }

        [HttpDelete("api/rims/{rimId}/photos/{id}")]
        public async Task<IActionResult> DeleteRimPhoto(int rimId, int id)
        {
            var photo = await photoRepository.GetRimPhoto(rimId, id);

            if (photo == null)
            {
                return NotFound();
            }

            photoRepository.RemoveRimPhoto(photo);
            var folderPath = Path.Combine(host.WebRootPath, "uploads");
            var storageResult = await photoService.RemovePhoto(folderPath + "/" + photo.FilePath, folderPath + "/" + photo.ThumbnailPath);

            return Ok(photo);
        }

    }
}