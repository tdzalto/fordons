using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using vega.Controllers.Resources;
using vega.Core;
using vega.Core.Models;
using vega.Persistance;

namespace vega.Controllers
{
    [Route("/api/tireDims")]
    public class TireDimController : Controller
    {
        private readonly ITireDimRepository context;
        private readonly IMapper mapper;
        private readonly ImportSettings importSettings;
        private readonly IHostingEnvironment host;
        private readonly ICsvService csvService;
        private readonly VegaDbContext coreContext;

        public TireDimController(IHostingEnvironment host, ICsvService csvService, ITireDimRepository context, VegaDbContext coreContext, IMapper mapper, IOptionsSnapshot<ImportSettings> options)
        {
            this.coreContext = coreContext;
            this.csvService = csvService;
            this.host = host;
            this.importSettings = options.Value;
            this.mapper = mapper;
            this.context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<TireDimResource>> GetTireDims()
        {
            var tireDim = await coreContext.TireDims.ToListAsync();

            return mapper.Map<List<TireDim>, List<TireDimResource>>(tireDim);
        }

        [HttpPost]
        public async Task<IActionResult> UploadCsv(IFormFile file)
        {
            //File validation
            if (file == null) return BadRequest("Null fil");
            if (file.Length == 0) return BadRequest("Tom fil");
            if (file.Length == importSettings.MaxBytes) return BadRequest("Max filstorlek uppnådd");
            if (!importSettings.IsSupported(file.FileName)) return BadRequest("Fel filtyp");


            List<string> records = new List<string>();
            using (StreamReader sr = new StreamReader(file.OpenReadStream(), Encoding.UTF8))
            {
                string result = sr.ReadToEnd();
                records = new List<string>(result.Split('\r'));
            }

            foreach (string record in records)
            {
                TireDim tireDim = new TireDim();
                string[] textpart = record.Split(';');
                try
                {
                    tireDim.Width = textpart[0];
                    tireDim.Height = textpart[1];
                    tireDim.Size = textpart[2];
                }
                catch (Exception e)
                {
                    throw e;
                }

                context.Add(tireDim);

            }

            await csvService.UploadCsv();
            return Ok();

        }
    }
}