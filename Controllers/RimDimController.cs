using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using vega.Controllers.Resources;
using vega.Core;
using vega.Core.Models;
using vega.Persistance;

namespace vega.Controllers
{
    [Route("/api/RimDims")]
    public class RimDimController : Controller
    {
        private readonly IRimDimRepository context;
        private readonly IMapper mapper;
        private readonly ImportSettings importSettings;
        private readonly IHostingEnvironment host;
        private readonly ICsvService csvService;
        public RimDimController(IHostingEnvironment host, ICsvService csvService, IRimDimRepository context, IMapper mapper, IOptionsSnapshot<ImportSettings> options)
        {
            this.csvService = csvService;
            this.host = host;
            this.importSettings = options.Value;
            this.mapper = mapper;
            this.context = context;
        }

        [HttpGet]
        public async Task<QueryResultResource<RimDimResource>> GetRimDims()
        {
            //var filter = mapper.Map<DimQueryResource, DimQuery>(filterResource);
            var rimDim = await context.GetRimDims();

            return mapper.Map<QueryResult<RimDim>, QueryResultResource<RimDimResource>>(rimDim);
        }

        [HttpPost]
        public async Task<IActionResult> UploadCsv(IFormFile file)
        {
            //File validation
            if (file == null) return BadRequest("Null fil");
            if (file.Length == 0) return BadRequest("Tom fil");
            if (file.Length == importSettings.MaxBytes) return BadRequest("Max filstorlek uppnådd");
            if (!importSettings.IsSupported(file.FileName)) return BadRequest("Fel filtyp");


            List<string> records = new List<string>();
            using (StreamReader sr = new StreamReader(file.OpenReadStream(), Encoding.UTF8))
            {
                string result = sr.ReadToEnd();
                records = new List<string>(result.Split('\r'));
            }

            foreach (string record in records)
            {
                RimDim rimDim = new RimDim();
                string[] textpart = record.Split(';');
                try
                {
                    rimDim.BoltCircle = textpart[0];
                    rimDim.BoltNutDim = textpart[1];
                    rimDim.Offset = textpart[2];
                    rimDim.Hub = textpart[3];
                }
                catch (Exception e)
                {
                    throw e;
                }

                context.Add(rimDim);

            }

            await csvService.UploadCsv();
            return Ok();

        }
    }
}