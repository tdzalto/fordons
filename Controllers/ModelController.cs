using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using vega.Controllers.Resources;
using vega.Core;
using vega.Core.Models;
using vega.Persistance;

namespace vega.Controllers
{
    [Route("/api/models")]
    public class ModelController : Controller
    {
        private readonly IModelRepository context;
        private readonly IMapper mapper;
        private readonly ImportSettings importSettings;
        private readonly IHostingEnvironment host;
        private readonly ICsvService csvService;
        public ModelController(IHostingEnvironment host, ICsvService csvService, IModelRepository context, IMapper mapper, IOptionsSnapshot<ImportSettings> options)
        {
            this.csvService = csvService;
            this.host = host;
            this.importSettings = options.Value;
            this.mapper = mapper;
            this.context = context;
        }

        [HttpGet]
        public async Task<QueryResultResource<ModelResource>> GetModels(ModelQueryResource filterResource)
        {
            var filter = mapper.Map<ModelQueryResource, ModelQuery>(filterResource);
            var model = await context.GetModels(filter);

            return mapper.Map<QueryResult<Model>, QueryResultResource<ModelResource>>(model);
        }

        [HttpPost]
        public async Task<IActionResult> UploadCsv(IFormFile file)
        {
            //File validation
            if (file == null) return BadRequest("Null fil");
            if (file.Length == 0) return BadRequest("Tom fil");
            if (file.Length == importSettings.MaxBytes) return BadRequest("Max filstorlek uppnådd");
            if (!importSettings.IsSupported(file.FileName)) return BadRequest("Fel filtyp");


            List<string> records = new List<string>();
            using (StreamReader sr = new StreamReader(file.OpenReadStream(), Encoding.UTF8))
            {
                string result = sr.ReadToEnd();
                records = new List<string>(result.Split('\r'));
            }

            foreach (string record in records)
            {
                Model model = new Model();
                string[] textpart = record.Split(';');
                try
                {
                    model.MakeId = Convert.ToInt32(textpart[0]);
                    model.Name = textpart[1];
                }
                catch (Exception e)
                {
                    throw e;
                }

                context.Add(model);

            }

            await csvService.UploadCsv();
            return Ok();

        }
    }
}