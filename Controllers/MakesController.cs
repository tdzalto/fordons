using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using vega.Controllers.Resources;
using vega.Core;
using vega.Core.Models;
using vega.Persistance;

namespace vega.Controllers
{
    [Route("/api/makes")]
    public class MakesController : Controller
    {
        private readonly VegaDbContext context;
        private readonly IMapper mapper;
        private readonly ImportSettings importSettings;
        private readonly ICsvService csvService;
        public MakesController(VegaDbContext context, IMapper mapper, IOptionsSnapshot<ImportSettings> options, ICsvService csvService)
        {
            this.csvService = csvService;
            this.importSettings = options.Value;
            this.mapper = mapper;
            this.context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<MakeResource>> GetMakes()
        {
            var makes = await context.Makes.Include(m => m.Models).ToListAsync();

            return mapper.Map<List<Make>, List<MakeResource>>(makes);
        }

        [HttpPost]
        public async Task<IActionResult> UploadCsv(IFormFile file)
        {
            System.Diagnostics.Debug.WriteLine(file);
            //File validation
            if (file == null) return BadRequest("Null fil");
            if (file.Length == 0) return BadRequest("Tom fil");
            if (file.Length == importSettings.MaxBytes) return BadRequest("Max filstorlek uppnådd");
            if (!importSettings.IsSupported(file.FileName)) return BadRequest("Fel filtyp");


            List<string> records = new List<string>();
            using (StreamReader sr = new StreamReader(file.OpenReadStream(), Encoding.UTF8))
            {
                string result = sr.ReadToEnd();
                records = new List<string>(result.Split('\r'));
            }

            foreach (string record in records)
            {
                Make make = new Make();
                string[] textpart = record.Split(';');
                try
                {
                    make.Name = textpart[0];
                }
                catch (Exception e)
                {
                    throw e;
                }

                context.Add(make);

            }

            await csvService.UploadCsv();
            return Ok();

        }

    }
}