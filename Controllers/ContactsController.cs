using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vega.Controllers.Resources;
using vega.Core.Models;
using vega.Core;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace vega.Controllers
{
    [Route("/api/contacts")]
    public class ContactsController : Controller
    {
        private readonly IMapper mapper;
        private readonly IContactRepository repository;
        private readonly IUnitOfWork unitOfWork;
        public ContactsController(IMapper mapper, IContactRepository repository, IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateContact([FromBody] SaveContactResource contactResource)
        {
            if (!ModelState.IsValid){
                return BadRequest(ModelState);
            }

            var contact = mapper.Map<SaveContactResource, Contact>(contactResource);
            contact.LastUpdate = DateTime.Now;

            repository.Add(contact);
            await unitOfWork.CompleteAsync();

            contact = await repository.GetContact(contact.Id);

            var result = mapper.Map<Contact, ContactResource>(contact);

            return Ok(result);
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> UpdateContact(int id, [FromBody] SaveContactResource contactResource)
        {
            if (!ModelState.IsValid){
                return BadRequest(ModelState);
            }

            var contact = await repository.GetContact(id);

            if (contact == null){
             return NotFound();
            }

            mapper.Map<SaveContactResource, Contact>(contactResource, contact);
            contact.LastUpdate = DateTime.Now;

            await unitOfWork.CompleteAsync();

            contact = await repository.GetContact(contact.Id);
            var result = mapper.Map<Contact, ContactResource>(contact);

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetContact(int id)
        {
            var contact = await repository.GetContact(id);

            if (contact == null){
             return NotFound();
            }

            var contactResource = mapper.Map<Contact, ContactResource>(contact);

            return Ok(contactResource);
        }

        [HttpGet("email/{email}")]
        [Authorize]
        public async Task<IActionResult> GetContactByEmail(string email)
        {

            var contact = await repository.GetContactByEmail(email);



            if (contact == null){
             return NotFound();
            }

            var contactResource = mapper.Map<Contact, ContactResource>(contact);

            return Ok(contactResource);
        }

        [HttpGet("token/{token}")]
        [Authorize]
        public async Task<IActionResult> GetContactByToken(string token)
        {

            var contact = await repository.GetContactByToken(token);



            if (contact == null){
             return NotFound();
            }

            var contactResource = mapper.Map<Contact, ContactResource>(contact);

            return Ok(contactResource);
        }

        [HttpGet]
        public async Task<QueryResultResource<ContactResource>> GetContacts(KeyValueQueryResource filterResource)
        {
            var filter = mapper.Map<KeyValueQueryResource, KeyValueQuery>(filterResource);
            var queryResult = await repository.GetContacts(filter);

            return mapper.Map<QueryResult<Contact>, QueryResultResource<ContactResource>>(queryResult);
        }
    }
}