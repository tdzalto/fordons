using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace vega.Controllers.Resources
{
    public class ModelResource : KeyValuePairResource
    {
        public KeyValuePairResource Make { get; set; }
        public ICollection<KeyValuePairResource> TireDims { get; set; }
        public ICollection<KeyValuePairResource> RimDims { get; set; }

        public ModelResource()
        {
            TireDims = new Collection<KeyValuePairResource>();
            RimDims = new Collection<KeyValuePairResource>();
        }
    }
}