namespace vega.Controllers.Resources
{
    public class PhotoResource // set up a resource (6)
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public string ThumbnailPath { get; set; }

    }
}