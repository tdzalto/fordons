using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using vega.Core.Models;

namespace vega.Controllers.Resources
{
    public class RimResource : KeyValuePairResource
    {
        public string Dimension { get; set; }
        public RimDimResource RimDim { get; set; }
        public int RimSize { get; set; }
        public string RimType { get; set; }
        public int ContactId { get; set; }
        public DateTime LastUpdate { get; set; }
        public ContactResource Contact { get; set; }

        public ICollection<PhotoResource> Photos { get; set; }
        public RimResource()
        {
          Photos = new Collection<PhotoResource>();
        }

    }
}