using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace vega.Controllers.Resources
{
    public class TireMakeResource : KeyValuePairResource
    {
        public ICollection<KeyValuePairResource> TireModels { get; set; }

        public TireMakeResource()
        {
            TireModels = new Collection<KeyValuePairResource>();
        }
    }
}