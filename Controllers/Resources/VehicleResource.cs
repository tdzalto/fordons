using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using vega.Core.Models;

namespace vega.Controllers.Resources
{
    public class VehicleResource : KeyValuePairResource
    {
        public KeyValuePairResource Model { get; set; }
        public KeyValuePairResource Make { get; set; }
        public bool IsRegistered { get; set; }
        public int ContactId { get; set; }
        public ContactResource Contact { get; set; }
        public DateTime LastUpdate { get; set; }
        public ICollection<KeyValuePairResource> Features { get; set; }
        public ICollection<PhotoResource> Photos { get; set; }
        public ICollection<KeyValuePairObjectResource> Tires { get; set; }
        public ICollection<KeyValuePairObjectResource> Rims { get; set; }
        public VehicleResource()
        {
            Features = new Collection<KeyValuePairResource>();
            Photos = new Collection<PhotoResource>();
            Tires = new Collection<KeyValuePairObjectResource>();
            Rims = new Collection<KeyValuePairObjectResource>();
        }

        //Car specifics
        public string regnr { get; set; }
        public string fullname { get; set; }
        public int vehicle_year { get; set; }
        public int model_year { get; set; }
        public string color { get; set; }
        public string chassi { get; set; }
        public string transmission { get; set; }
        public string fuel_1 { get; set; }
        public string consumption_1 { get; set; }
        public int power_kw_1 { get; set; }
        public int power_hp_1 { get; set; }
        public string co2_1 { get; set; }
        public string nox_1 { get; set; }
        public int sound_level_1 { get; set; }
        public string imported { get; set; }
        public string first_registered { get; set; }
        public string first_on_swedish_roads { get; set; }
        public string purchased { get; set; }
        public int number_of_owners { get; set; }
        public int meter { get; set; }
        public string lastest_inspection { get; set; }
        public string inspection_valid_until { get; set; }
        public int tax { get; set; }
        public int cylinder_volume { get; set; }
        public string top_speed { get; set; }
        public string four_wheel_drive { get; set; }
        public int number_of_passengers { get; set; }
        public int length { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int kerb_weight { get; set; }
        public int total_weight { get; set; }
        public int load_weight { get; set; }
        public string passenger_airbag { get; set; }
    }
}