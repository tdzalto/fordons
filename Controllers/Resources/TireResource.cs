using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using vega.Core.Models;

namespace vega.Controllers.Resources
{
    public class TireResource : KeyValuePairResource
    {
        public string Dimension { get; set; }
        public KeyValuePairResource TireModel { get; set; }
        public KeyValuePairResource TireMake { get; set; }
        public int ContactId { get; set; }
        public TireDimResource TireDim { get; set; }
        public string TireType { get; set; }
        public DateTime LastUpdate { get; set; }
        public ContactResource Contact { get; set; }
        public ICollection<PhotoResource> Photos { get; set; }
        public TireResource()
        {
          Photos = new Collection<PhotoResource>();
        }

    }
}