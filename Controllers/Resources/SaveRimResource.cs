using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace vega.Controllers.Resources
{
    public class SaveRimResource : KeyValuePairResource
    {
        public string RimDimId { get; set; }
        public string RimType { get; set; }
        public int RimSize { get; set; }
        public int ContactId { get; set; }
        public DateTime LastUpdate { get; set; }

    }
}