using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace vega.Controllers.Resources
{
    public class ContactResource
    {
        public int Id { get; set; }
        
        public string Token { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        [StringLength(5000)]
        public string Description { get; set; }


        public ICollection<KeyValuePairResource> Vehicles { get; set; }
        public ICollection<KeyValuePairResource> Tires { get; set; }
        public ICollection<RimResource> Rims { get; set; }
        public ContactResource()
        {
            Vehicles = new Collection<KeyValuePairResource>();
            Tires = new Collection<KeyValuePairResource>();
            Rims = new Collection<RimResource>();

        }
    }
}