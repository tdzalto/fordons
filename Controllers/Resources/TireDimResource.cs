using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using vega.Core.Models;

namespace vega.Controllers.Resources
{
    public class TireDimResource
    {
        public int Id { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Size { get; set; }
        public string Name { get; set; }

        public ICollection<KeyValuePairResource> Tires  { get; set; }

        public TireDimResource()
        {
            Tires = new Collection<KeyValuePairResource>();
        }

    }
}