using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using vega.Core.Models;

namespace vega.Controllers.Resources
{
    public class RimDimResource
    {
        public int Id { get; set; }
        public string Hub { get; set; }
        public string BoltCircle { get; set; }
        public string BoltNutDim { get; set; }
        public string Offset { get; set; }
        public string Name { get; set; }
        public ICollection<KeyValuePairResource> Rims  { get; set; }

        public RimDimResource()
        {
            Rims = new Collection<KeyValuePairResource>();
        }

    }
}