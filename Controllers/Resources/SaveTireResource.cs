using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace vega.Controllers.Resources
{
    public class SaveTireResource
    {
        public int TireModelId { get; set; }
        public string TireType { get; set; }
        public int TireDimId { get; set; }
        public int ContactId { get; set; }
        public DateTime LastUpdate { get; set; }

    }
}