using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vega.Controllers.Resources;
using vega.Core.Models;
using vega.Core;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace vega.Controllers
{
    [Route("/api/rims")]
    public class RimsController : Controller
    {
        private readonly IMapper mapper;
        private readonly IRimRepository repository;
        private readonly IUnitOfWork unitOfWork;
        public RimsController(IMapper mapper, IRimRepository repository, IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateRim([FromBody] SaveRimResource rimResource)
        {
            if (!ModelState.IsValid){
                return BadRequest(ModelState);
            }

            var rim = mapper.Map<SaveRimResource, Rim>(rimResource);
            rim.LastUpdate = DateTime.Now;

            repository.Add(rim);
            await unitOfWork.CompleteAsync();

            rim = await repository.GetRim(rim.Id);

            var result = mapper.Map<Rim, RimResource>(rim);

            return Ok(result);
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> UpdateRim(int id, [FromBody] SaveRimResource rimResource)
        {
            if (!ModelState.IsValid){
                return BadRequest(ModelState);
            }

            var rim = await repository.GetRim(id);

            if (rim == null) {
             return NotFound();
            }

            mapper.Map<SaveRimResource, Rim>(rimResource, rim);
            rim.LastUpdate = DateTime.Now;

            await unitOfWork.CompleteAsync();

            rim = await repository.GetRim(rim.Id);
            var result = mapper.Map<Rim, RimResource>(rim);

            return Ok(result);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteRim(int id)
        {
            var rim = await repository.GetRim(id, includeRelated: false);

            if (rim == null){
             return NotFound();
            }

            repository.Remove(rim);
            await unitOfWork.CompleteAsync();

            return Ok(rim);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetRim(int id)
        {
            var rim = await repository.GetRim(id);

            if (rim == null) {
             return NotFound();
            }

            var rimResource = mapper.Map<Rim, RimResource>(rim);

            return Ok(rimResource);
        }

        [HttpGet]
        public async Task<QueryResultResource<RimResource>> GetRims(KeyValueQueryResource filterResource)
        {
            var filter = mapper.Map<KeyValueQueryResource, KeyValueQuery>(filterResource);
            Console.WriteLine(filter);
            var queryResult = await repository.GetRims(filter);

            return mapper.Map<QueryResult<Rim>, QueryResultResource<RimResource>>(queryResult);
        }
    }
}