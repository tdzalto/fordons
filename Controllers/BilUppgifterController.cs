using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;

namespace vega.Controllers
{
    [Route("/api/fordons")]
    public class BilUppgifterController : Controller
    {
        [HttpGet("{regnr}")]
        public async Task<IActionResult> SearchBilUppgifter(string regnr)
        {
            var result = await Get("https://biluppgifter.se/api/fordon/" + regnr);
            if (result == null)
            {
                return NotFound(regnr);
            }
            return Ok(result);
        }

        public async Task<dynamic> Get(string url)
        {
            //Generate a webRequest using the URL
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "GET";

            //Asks for data from the server
            using (HttpWebResponse response = await request.GetResponseAsync() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());

                //Place result into a string
                string result = await reader.ReadToEndAsync();

                //Convert result to JSON Object
                dynamic jsonObj = JsonConvert.DeserializeObject<dynamic>(result);

                return (result);
            }

        }
    }
}