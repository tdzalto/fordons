using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vega.Controllers.Resources;
using vega.Core.Models;
using vega.Persistance;

namespace vega.Controllers
{
    public class TireMakesController : Controller
    {
        private readonly VegaDbContext context;
        private readonly IMapper mapper;
        public TireMakesController(VegaDbContext context, IMapper mapper)
        {
            this.mapper = mapper;
            this.context = context;
        }

        [HttpGet("/api/tireMakes")]
        public async Task<IEnumerable<TireMakeResource>> GetTireMakes()
        {
            var tireMakes = await context.TireMakes.Include(tm => tm.TireModels).ToListAsync();

            return mapper.Map<List<TireMake>, List<TireMakeResource>>(tireMakes);
        }
    }
}